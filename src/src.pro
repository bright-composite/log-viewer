TEMPLATE = app

QT += qml quick widgets

CONFIG += c++11

TARGET = "Log Viewer"

SOURCES += \
	$$PWD/main.cpp \
	$$PWD/models/EventListModel.cpp \
	$$PWD/services/AsyncModelWorker.cpp \
	$$PWD/adapters/ModelAdapter.cpp \
	$$PWD/adapters/ApacheErrorLogAdapter.cpp \
	$$PWD/services/ModelSource.cpp

HEADERS += \
	$$PWD/models/EventListModel.h \
	$$PWD/exceptions/FieldParseException.h \
	$$PWD/exceptions/RecordParseException.h \
	$$PWD/models/SortFilterOptions.h \
	$$PWD/models/ModelPatch.h \
	$$PWD/utils/Escape.h \
	$$PWD/models/SearchResults.h \
	$$PWD/services/AsyncModelWorker.h \
	$$PWD/adapters/ApacheErrorLogAdapter.h \
	$$PWD/adapters/ModelAdapter.h \
	$$PWD/services/ModelSource.h

INCLUDEPATH += $$PWD

RESOURCES += \
	$$PWD/../assets/assets.qrc

include($$PWD/../qml.pri)

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
