import QtQuick 2.0

Item {
    anchors.fill: parent

    property bool enableLeft: true
    property bool enableTop: true
    property bool enableRight: true
    property bool enableBottom: true

    property var colors: [
        "#10ffffff",
        "#10ffffff",
        "#10ffffff",
        "#10ffffff"
    ]

    Rectangle {
        anchors {
            left: parent.left
            top: parent.top
            bottom: parent.bottom
        }

        width: 1
        color: colors[0 % colors.length]
        visible: enableLeft
    }

    Rectangle {
        anchors {
            right: parent.right
            top: parent.top
            bottom: parent.bottom
        }

        width: 1
        color: colors[1 % colors.length]
        visible: enableRight
    }

    Rectangle {
        anchors {
            left: parent.left
            top: parent.top
            right: parent.right
        }

        height: 1
        color: colors[2 % colors.length]
        visible: enableTop
    }

    Rectangle {
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        height: 1
        color: colors[3 % colors.length]
        visible: enableBottom
    }
}
