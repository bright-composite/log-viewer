import QtQuick 2.0

import ui 1.0

Item {
    property var table

    anchors.fill: parent

    Rectangle {
        anchors {
            left: parent.left
            top: parent.top
            bottom: parent.bottom
            topMargin: 4
            bottomMargin: 4
        }

        width: 1
        color: "#10ffffff"
        visible: table && table.hasResizer(styleData.column - 1)
    }

    Rectangle {
        anchors {
            right: parent.right
            top: parent.top
            bottom: parent.bottom
            topMargin: 4
            bottomMargin: 4
        }

        width: 1
        color: "#10000000"
        visible: table && table.hasResizer(styleData.column)
    }
}
