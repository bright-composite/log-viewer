import QtQuick 2.0

MouseArea {
    id: area

    property string text

    anchors.fill: parent
    z: 10000

    hoverEnabled: true
    propagateComposedEvents: true

    onEntered: {
        var obj = area.mapToItem(tooltip.parent, 0, 0)
        tooltip.text = Qt.binding(function() { return area.text })
        tooltip.x = (obj.x + area.width - 10 + tooltip.content.width >= tooltip.parent.width) ? obj.x - tooltip.content.width : obj.x + area.width - 10
        tooltip.y = (obj.y + area.height - 10 + tooltip.content.height >= tooltip.parent.height) ? obj.y - 10 - tooltip.content.height : obj.y + area.height - 10

        tooltip.shown = true
    }

    onExited: {
        tooltip.shown = false
    }
}
