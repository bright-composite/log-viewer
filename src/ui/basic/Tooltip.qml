import QtQuick 2.5
import QtQuick.Controls 1.1
import QtGraphicalEffects 1.0

import ui 1.0

Item {
    id: tooltip

    width: tooltipText.contentWidth
    height: container.height

    parent: root.contentItem

    clip: false
    z: 9999

    property string text
    property bool shown: false
    property alias content: content

    opacity: text && shown ? 1.0 : 0.0

    DropShadow {
        id: toolTipShadow

        z: tooltip.z + 1
        anchors.fill: container
        horizontalOffset: 4
        verticalOffset: 4
        radius: 8.0
        samples: 16
        color: "#80000000"
        smooth: true
        source: container
    }

    Item {
        id: container
        z: tooltip.z + 1
        width: content.width + (2 * toolTipShadow.radius)
        height: content.height + (2 * toolTipShadow.radius)

        Rectangle {
            id: content
            anchors.centerIn: parent
            width: tooltipText.contentWidth + 10
            height: tooltipText.contentHeight + 10
            radius: 3
            color: "#212121"

            Label {
                id: tooltipText

                anchors {
                    left: parent.left
                    top: parent.top
                    margins: 5
                }

                color: "#fff"
                text: tooltip.text
                wrapMode: Text.WrapAnywhere
            }
        }
    }

    Behavior on opacity {
        enabled: Config.enableAnimations

        OpacityAnimator {
            duration: 100
        }
    }
}
