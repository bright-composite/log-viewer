import QtQuick 2.0

Item {
    id: item

    property int minimumWidth: 300
    property int minimumHeight: 300
    property int maximumWidth: -1
    property int maximumHeight: -1
    property int borderSize: 10

    property bool movable: true
    property bool resizable: true

    property ResizeSides resize: ResizeSides {}

    onMinimumWidthChanged: {
        if(implicitWidth < minimumWidth) {
            implicitWidth = minimumWidth
        }
    }

    onMinimumHeightChanged: {
        if(implicitHeight < minimumHeight) {
            implicitHeight = minimumHeight
        }
    }

    onMaximumWidthChanged: {
        if(maximumWidth > 0 && implicitWidth > maximumWidth && maximumWidth > minimumWidth) {
            implicitWidth = maximumWidth
        }
    }

    onMaximumHeightChanged: {
        if(maximumHeight > 0 && implicitHeight > maximumHeight && maximumHeight > minimumHeight) {
            implicitHeight = maximumHeight
        }
    }

    // side resizers

    Item {
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
            topMargin: borderSize / 2
            bottomMargin: borderSize / 2
            leftMargin: -borderSize / 2
        }

        z: 200
        width: borderSize
        visible: resize.left

        MouseArea {
            anchors.fill: parent
            drag {
                target: parent
                axis: Drag.XAxis
            }

            cursorShape: Qt.SizeHorCursor

            onMouseXChanged: {
                if(drag.active && resize.left) {
                    if(item.implicitWidth - mouseX < minimumWidth) {
                        if(movable) {
                            item.x = item.x + item.implicitWidth - minimumWidth
                        }

                        item.implicitWidth = minimumWidth
                    } else if(maximumWidth > 0 && item.implicitWidth - mouseX > maximumWidth) {
                        if(movable) {
                            item.x = item.x + item.implicitWidth - maximumWidth
                        }

                        item.implicitWidth = maximumWidth
                    } else {
                        if(movable) {
                            item.x = item.x + mouseX
                        }

                        item.implicitWidth = item.implicitWidth - mouseX
                    }
                }
            }
        }
    }

    Item {
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            leftMargin: borderSize / 2
            rightMargin: borderSize / 2
            topMargin: -borderSize / 2
        }

        z: 200
        height: borderSize
        visible: resize.top

        MouseArea {
            anchors.fill: parent
            drag {
                target: parent
                axis: Drag.YAxis
            }

            cursorShape: Qt.SizeVerCursor

            onMouseYChanged: {
                if(drag.active && resize.top) {
                    if(item.implicitHeight - mouseY < minimumHeight) {
                        if(movable) {
                            item.y = item.y + item.implicitHeight - minimumHeight
                        }

                        item.implicitHeight = minimumHeight
                    } else if(maximumHeight > 0 && item.implicitHeight - mouseY > maximumHeight) {
                        if(movable) {
                            item.y = item.y + item.implicitHeight - maximumHeight
                        }

                        item.implicitHeight = maximumHeight
                    } else {
                        if(movable) {
                            item.y = item.y + mouseY
                        }

                        item.implicitHeight = item.implicitHeight - mouseY

                        if(item.y < 0) {
                            item.implicitHeight += item.y
                            item.y = 0
                        }
                    }
                }
            }
        }
    }

    Item {
        anchors {
            top: parent.top
            bottom: parent.bottom
            right: parent.right
            topMargin: borderSize / 2
            bottomMargin: borderSize / 2
            rightMargin: -borderSize / 2
        }

        z: 200
        width: borderSize
        visible: resize.right

        MouseArea {
            anchors.fill: parent
            drag {
                target: parent
                axis: Drag.XAxis
            }

            cursorShape: Qt.SizeHorCursor

            onMouseXChanged: {
                if(drag.active && resize.right) {
                    if(item.implicitWidth + mouseX < minimumWidth) {
                        item.implicitWidth = minimumWidth
                    } else if(maximumWidth > 0 && item.implicitWidth + mouseX > maximumWidth) {
                        item.implicitWidth = maximumWidth
                    } else {
                        item.implicitWidth = item.implicitWidth + mouseX
                    }
                }
            }
        }
    }

    Item {
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            leftMargin: borderSize / 2
            rightMargin: borderSize / 2
            bottomMargin: -borderSize / 2
        }

        z: 200
        height: borderSize
        visible: resize.bottom

        MouseArea {
            anchors.fill: parent
            drag {
                target: parent
                axis: Drag.YAxis
            }

            cursorShape: Qt.SizeVerCursor

            onMouseYChanged: {
                if(drag.active && resize.bottom) {
                    if(item.implicitHeight + mouseY < minimumHeight) {
                        item.implicitHeight = minimumHeight
                    } else if(maximumHeight > 0 && item.implicitHeight + mouseY > maximumHeight) {
                        item.implicitHeight = maximumHeight
                    } else {
                        item.implicitHeight = item.implicitHeight + mouseY
                    }
                }
            }
        }
    }

    // corner resizers

    Item {
        anchors {
            left: parent.left
            top: parent.top
            leftMargin: -borderSize / 2
            topMargin: -borderSize / 2
        }

        z: 200
        width: borderSize
        height: borderSize
        visible: resize.leftTop

        MouseArea {
            anchors.fill: parent
            drag {
                target: parent
                axis: resize.top && resize.left ? Drag.XAndYAxis : resize.left ? Drag.XAxis : resize.top ? Drag.YAxis : Drag.None
            }

            cursorShape: Qt.SizeFDiagCursor

            onMouseXChanged: {
                if(drag.active && resize.left) {
                    if(item.implicitWidth - mouseX < minimumWidth) {
                        if(movable) {
                            item.x = item.x + item.implicitWidth - minimumWidth
                        }

                        item.implicitWidth = minimumWidth
                    } else if(maximumWidth > 0 && item.implicitWidth - mouseX > maximumWidth) {
                        if(movable) {
                            item.x = item.x + item.implicitWidth - maximumWidth
                        }

                        item.implicitWidth = maximumWidth
                    } else {
                        if(movable) {
                            item.x = item.x + mouseX
                        }

                        item.implicitWidth = item.implicitWidth - mouseX
                    }
                }
            }

            onMouseYChanged: {
                if(drag.active && resize.top) {
                    if(item.implicitHeight - mouseY < minimumHeight) {
                        if(movable) {
                            item.y = item.y + item.implicitHeight - minimumHeight
                        }

                        item.implicitHeight = minimumHeight
                    } else if(maximumHeight > 0 && item.implicitHeight - mouseY > maximumHeight) {
                        if(movable) {
                            item.y = item.y + item.implicitHeight - maximumHeight
                        }

                        item.implicitHeight = maximumHeight
                    } else {
                        if(movable) {
                            item.y = item.y + mouseY
                        }

                        item.implicitHeight = item.implicitHeight - mouseY

                        if(item.y < 0) {
                            item.implicitHeight += item.y
                            item.y = 0
                        }
                    }
                }
            }
        }
    }

    Item {
        anchors {
            right: parent.right
            top: parent.top
            rightMargin: -borderSize / 2
            topMargin: -borderSize / 2
        }

        z: 200
        width: borderSize
        height: borderSize
        visible: resize.rightTop

        MouseArea {
            anchors.fill: parent
            drag {
                target: parent
                axis: resize.top && resize.right ? Drag.XAndYAxis : resize.right ? Drag.XAxis : resize.top ? Drag.YAxis : Drag.None
            }

            cursorShape: Qt.SizeBDiagCursor

            onMouseXChanged: {
                if(drag.active && resize.right) {
                    if(item.implicitWidth + mouseX < minimumWidth) {
                        item.implicitWidth = minimumWidth
                    } else if(maximumWidth > 0 && item.implicitWidth + mouseX > maximumWidth) {
                        item.implicitWidth = maximumWidth
                    } else {
                        item.implicitWidth = item.implicitWidth + mouseX
                    }
                }
            }

            onMouseYChanged: {
                if(drag.active && resize.top) {
                    if(item.implicitHeight - mouseY < minimumHeight) {
                        if(movable) {
                            item.y = item.y + item.implicitHeight - minimumHeight
                        }

                        item.implicitHeight = minimumHeight
                    } else if(maximumHeight > 0 && item.implicitHeight - mouseY > maximumHeight) {
                        if(movable) {
                            item.y = item.y + item.implicitHeight - maximumHeight
                        }

                        item.implicitHeight = maximumHeight
                    } else {
                        if(movable) {
                            item.y = item.y + mouseY
                        }

                        item.implicitHeight = item.implicitHeight - mouseY

                        if(item.y < 0) {
                            item.implicitHeight += item.y
                            item.y = 0
                        }
                    }
                }
            }
        }
    }

    Item {
        anchors {
            right: parent.right
            bottom: parent.bottom
            rightMargin: -borderSize / 2
            bottomMargin: -borderSize / 2
        }

        z: 200
        width: borderSize
        height: borderSize
        visible: resize.rightBottom

        MouseArea {
            anchors.fill: parent
            drag {
                target: parent
                axis: resize.bottom && resize.right ? Drag.XAndYAxis : resize.right ? Drag.XAxis : resize.bottom ? Drag.YAxis : Drag.None
            }

            cursorShape: Qt.SizeFDiagCursor

            onMouseXChanged: {
                if(drag.active && resize.right) {
                    if(item.implicitWidth + mouseX < minimumWidth) {
                        item.implicitWidth = minimumWidth
                    } else if(maximumWidth > 0 && item.implicitWidth + mouseX > maximumWidth) {
                        item.implicitWidth = maximumWidth
                    } else {
                        item.implicitWidth = item.implicitWidth + mouseX
                    }
                }
            }

            onMouseYChanged: {
                if(drag.active && resize.bottom) {
                    if(item.implicitHeight + mouseY < minimumHeight) {
                        item.implicitHeight = minimumHeight
                    } else if(maximumHeight > 0 && item.implicitHeight + mouseY > maximumHeight) {
                        item.implicitHeight = maximumHeight
                    } else {
                        item.implicitHeight = item.implicitHeight + mouseY
                    }
                }
            }
        }
    }

    Item {
        anchors {
            left: parent.left
            bottom: parent.bottom
            leftMargin: -borderSize / 2
            bottomMargin: -borderSize / 2
        }

        z: 200
        width: borderSize
        height: borderSize
        visible: resize.leftBottom

        MouseArea {
            anchors.fill: parent
            drag {
                target: parent
                axis: resize.bottom && resize.left ? Drag.XAndYAxis : resize.left ? Drag.XAxis : resize.bottom ? Drag.YAxis : Drag.None
            }

            cursorShape: Qt.SizeBDiagCursor

            onMouseXChanged: {
                if(drag.active && resize.left) {
                    if(item.implicitWidth - mouseX < minimumWidth) {
                        if(movable) {
                            item.x = item.x + item.implicitWidth - minimumWidth
                        }

                        item.implicitWidth = minimumWidth
                    } else if(maximumWidth > 0 && item.implicitWidth - mouseX > maximumWidth) {
                        if(movable) {
                            item.x = item.x + item.implicitWidth - maximumWidth
                        }

                        item.implicitWidth = maximumWidth
                    } else {
                        if(movable) {
                            item.x = item.x + mouseX
                        }

                        item.implicitWidth = item.implicitWidth - mouseX
                    }
                }
            }

            onMouseYChanged: {
                if(drag.active && resize.bottom) {
                    if(item.implicitHeight + mouseY < minimumHeight) {
                        item.implicitHeight = minimumHeight
                    } else if(maximumHeight > 0 && item.implicitHeight + mouseY > maximumHeight) {
                        item.implicitHeight = maximumHeight
                    } else {
                        item.implicitHeight = item.implicitHeight + mouseY
                    }
                }
            }
        }
    }
}
