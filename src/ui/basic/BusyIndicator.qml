import QtQuick 2.5

Item {
    id: item

    property bool active: false
    property color color: "#fff"

    opacity: active ? 1.0 : 0.0

    onActiveChanged: {
        if(active) {
            timer.progress = 0.0
        }
    }

    Timer {
        id: timer

        property real progress: 0.0

        running: item.active
        interval: 5
        repeat: true
        onTriggered: {
            timer.progress += 0.001

            if(timer.progress >= 1.0) {
                timer.progress = 0.0
            }

            canvas.requestPaint()
        }
    }

    Canvas {
        id: canvas

        anchors.fill: parent

        readonly property real period: Math.PI * 4

        onPaint: {
            var ctx = canvas.getContext("2d")
            ctx.lineWidth = 2
            ctx.lineCap = "round"
            ctx.strokeStyle = item.color

            ctx.clearRect(0, 0, width, height)

            var a = timer.progress * period * 5
            var b = (Math.cos(timer.progress * period) + 1.1) * Math.PI * 0.8

            ctx.beginPath()
            ctx.arc(width / 2, height / 2, Math.min(width, height) / 2 - 3, a, a + b)
            ctx.stroke()
        }
    }
}
