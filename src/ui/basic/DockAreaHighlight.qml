import QtQuick 2.5

import ui 1.0

Rectangle {
    property int position: Qt.AlignCenter

    readonly property int orientation: position == Qt.AlignLeft || position == Qt.AlignRight ? Qt.Vertical : position == Qt.AlignTop || position == Qt.AlignBottom ? Qt.Horizontal : 3
    readonly property bool active: dock.currentDockable && dock.currentDockable.dragPosition == position

    width: orientation == Qt.Horizontal ? active ? parent.width : 200 : active ? dock.currentDockable.target.implicitWidth : 100
    height: orientation == Qt.Vertical ? active ? parent.height : 200 : active ? dock.currentDockable.target.implicitHeight : 100
    color: Style.color.highlight

    opacity: dock.currentDockable && (dock.currentDockable.dragPosition == position || dock.currentDockable.dragPosition == Qt.AlignCenter) ? active ? 1.0 : 0.5 : 0.0

    z: dock.currentDockable && dock.currentDockable.position == Qt.AlignCenter ? 100 : 0

    Behavior on opacity {
        enabled: Config.enableAnimations

        OpacityAnimator {
            duration: 200
        }
    }

    Behavior on width {
        enabled: Config.enableAnimations

        NumberAnimation {
            duration: 200
        }
    }

    Behavior on height {
        enabled: Config.enableAnimations

        NumberAnimation {
            duration: 200
        }
    }

    border {
        width: 2
        color: Qt.tint(Style.color.highlight, Style.color.highlight)
    }
}
