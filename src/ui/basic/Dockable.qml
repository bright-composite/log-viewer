import QtQuick 2.0

Item {
    id: dockable

    property Item target
    property DockArea dockArea

    property int lastPosition: Qt.AlignRight
    property int position: Qt.AlignRight
    property int dragPosition: dockArea.classify(target.x + dragArea.mouseX - dockArea.width / 2, target.y + dragArea.mouseY - dockArea.height / 2)

    property alias dragArea: dragArea

    readonly property int orientation: position == Qt.AlignLeft || position == Qt.AlignRight ? Qt.Vertical : position == Qt.AlignTop || position == Qt.AlignBottom ? Qt.Horizontal : 3
    readonly property bool dragging: dragArea.drag.active
    readonly property bool docked: position != Qt.AlignCenter
    readonly property bool undocked: position == Qt.AlignCenter

    Binding {
        target: dockable.target
        property: "width"
        value: dockable.orientation != Qt.Horizontal || dragging ? dockable.target.implicitWidth : dockArea.width
    }

    Binding {
        target: dockable.target
        property: "height"
        value: dockable.orientation != Qt.Vertical || dragging ? dockable.target.implicitHeight : dockArea.height
    }

    Binding {
        target: dockable.target
        property: "z"
        value: dragArea.drag.active ? 100 : 0
    }

    Binding {
        target: dockable.target
        property: "parent"
        value: dockable.selectParent(dockable.position)
    }

    Connections {
        target: dockArea

        onWidthChanged: {
            if(dockable.target.x + dockable.target.implicitWidth / 2 > dockArea.width) {
                dockable.target.x = Math.max(dockArea.width, 0) - dockable.target.implicitWidth / 2
            }
        }

        onHeightChanged: {
            if(dockable.target.y + dockable.dragArea.height > dockArea.height) {
                dockable.target.y = Math.max(dockArea.height - dockable.dragArea.height, 0)
            }
        }
    }

    onPositionChanged: {
        if(position != Qt.AlignCenter) {
            lastPosition = position
            target.x = 0
            target.y = 0
        }
    }

    function selectParent(position) {
        switch(position) {
            case Qt.AlignTop:
                return dockArea.sideTop

            case Qt.AlignLeft:
                return dockArea.sideLeft

            case Qt.AlignRight:
                return dockArea.sideRight

            case Qt.AlignBottom:
                return dockArea.sideBottom
        }

        return dockArea
    }

    MouseArea {
        id: dragArea

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }

        parent: target
        cursorShape: dragArea.pressed ? Qt.ClosedHandCursor : Qt.OpenHandCursor

        drag {
            minimumY: 0
            minimumX: -target.width / 2
            maximumX: dockArea.width - target.width / 2
            maximumY: dockArea.height - dragArea.height
        }

        /**
         * Timers are used to prevent ambiguity between click and press events
         */

        Timer {
            id: dragTimer
            interval: 100

            onTriggered: {
                var pos = target.parent.mapToItem(dockArea, target.x, target.y)
                var coords = target.mapToItem(dockArea, dragArea.mouseX, dragArea.mouseY)

                // Ensure that mouse pointer will be in the drag area bounds
                target.x = Math.min(coords.x - target.implicitWidth * 0.2, Math.max(coords.x - target.implicitWidth * 0.8, pos.x))
                target.y = Math.min(coords.y, Math.max(coords.y - dragArea.height, pos.y))
                dockable.position = Qt.AlignCenter

                dragArea.drag.target = target
            }
        }

        Timer {
            id: clickTimer
            interval: 100

            onTriggered: {
                target.x = 0
                target.y = 0
                dockable.position = dockable.lastPosition
            }
        }

        /**
         * Start drag
         */
        onPressed: {
            dragTimer.start()
            dockable.target.forceActiveFocus()
        }

        /**
         * End drag (if it happened)
         */
        onReleased: {
            if(drag.target) {
                dockable.position = dragPosition

                if(dockable.position != Qt.AlignCenter) {
                    target.x = 0
                    target.y = 0
                }

                drag.target = undefined
            }

            dragTimer.stop()
        }

        /**
         * Return to dock
         */
        onDoubleClicked: {
            if(dockable.position == Qt.AlignCenter) {
                clickTimer.start()
            }
        }
    }
}
