import QtQuick 2.5

Item {
    id: dock

    property var currentDockable

    property alias sideTop: sideTop
    property alias sideLeft: sideLeft
    property alias sideRight: sideRight
    property alias sideBottom: sideBottom

    Item {
        id: sideTop

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }

        height: children.length > 0 && children[0].visible ? children[0].height : 0
    }

    Item {
        id: sideLeft

        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
        }

        width: children.length > 0 && children[0].visible ? children[0].width : 0
    }

    Item {
        id: sideRight

        anchors {
            top: parent.top
            bottom: parent.bottom
            right: parent.right
        }

        width: children.length > 0 && children[0].visible ? children[0].width : 0
    }

    Item {
        id: sideBottom

        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        height: children.length > 0 && children[0].visible ? children[0].height : 0
    }

    Rectangle {
        anchors.fill: parent
        color: "#20000000"

        z: 100

        opacity: dock.currentDockable ? 1.0 : 0.0

        Behavior on opacity {
            OpacityAnimator {
                duration: 100
            }
        }
    }

    DockAreaHighlight {
        anchors {
            top: parent.top
            horizontalCenter: parent.horizontalCenter
        }

        position: Qt.AlignTop
    }

    DockAreaHighlight {
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
        }

        position: Qt.AlignLeft
    }

    DockAreaHighlight {
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
        }

        position: Qt.AlignRight
    }

    DockAreaHighlight {
        anchors {
            bottom: parent.bottom
            horizontalCenter: parent.horizontalCenter
        }

        position: Qt.AlignBottom
    }

    function classify(x, y) {
        if(Math.abs(y) < 100 && Math.abs(x) > (dock.width / 2 - 100) ||
           Math.abs(x) < 100 && Math.abs(y) > (dock.height / 2 - 100)) {
            if(x > y) {
                if(x < -y) {
                    return Qt.AlignTop
                }

                return Qt.AlignRight
            } else {
                if(x > -y) {
                    return Qt.AlignBottom
                }

                return Qt.AlignLeft
            }
        }

        return Qt.AlignCenter
    }
}
