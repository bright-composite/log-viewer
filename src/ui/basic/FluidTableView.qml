import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import "."

TableView {
    id: view

    property var columns: []

    property bool changingWidth: false
    readonly property int minimumViewportWidth: view.columns.reduce(function(acc, column) { return acc + column.minimumWidth }, 0)

    onWidthChanged: {
        if(width < flickableItem.contentWidth) {
            flickableItem.contentX = Math.min(flickableItem.contentX, flickableItem.contentWidth - width)
        }
    }

    viewport {
        onWidthChanged: {
            view.updateWidth()
        }
    }

    Component {
        id: columnScheme

        TableViewColumn {
            property int minimumWidth: 100
        }
    }

    function getSubsum() {
        return Array.apply(null, {length: view.columns.length - 1}).map(Function.call, Number).reduce(function(acc, i) { return acc + view.getColumn(i).width }, 0)
    }

    function updateWidth() {
        if(view.columnCount == 0 || view.columnCount != view.columns.length) {
            return;
        }

        view.changingWidth = true

        for(var i = 0; i < view.columns.length; ++i) {
            view.getColumn(i).width = Math.max(view.columns[i].minimumWidth, view.getColumn(i).width)
        }

        var sum = view.getSubsum()
        var lastMinimumWidth = view.columns[view.columns.length - 1].minimumWidth

        while(view.viewport.width - lastMinimumWidth < sum) {
            var all = 0
            var subtract = lastMinimumWidth

            for(var i = 0; i < view.columns.length - 1; ++i) {
                var column = view.getColumn(i)
                if(column.width > view.columns[i].minimumWidth) {
                    all += column.width
                } else {
                    subtract += view.columns[i].minimumWidth
                }
            }

            var q = (view.viewport.width - subtract) / all

            for(var i = 0; i < view.columns.length - 1; ++i) {
                var column = view.getColumn(i)
                column.width = Math.max(view.columns[i].minimumWidth, Math.floor(column.width * q))
            }

            sum = getSubsum()

            if(view.viewport.width < minimumViewportWidth) {
                break;
            }
        }

        view.getColumn(view.columns.length - 1).width = Math.max(view.viewport.width - sum, lastMinimumWidth)
        view.changingWidth = false
    }

    onColumnsChanged: {
        while(view.columnCount > 0) {
            view.removeColumn(0)
        }

        for(var i = 0; i < view.columns.length; ++i) {
            var column = view.columns[i]

            if(i == view.columns.length - 1) {
                column.resizable = false
            }

            var c = view.addColumn(columnScheme.createObject(view, column))

            c.widthChanged.connect(function() {
                if(!view.changingWidth) {
                    view.updateWidth()
                }
            })
        }
    }
}
