import QtQuick 2.0
import QtQuick.Controls 1.4

import ui 1.0

ScrollView {
    id: scroll

    anchors {
        top: parent.top
        left: parent.left
    }

    style: ScrollStyle {}

    // prevent wheel blocking when content sizes are less than scroll view sizes

    width: parent.width
    height: Math.min(parent.height, contentItem.height + 10)

    horizontalScrollBarPolicy: contentItem.width > scroll.width ? Qt.ScrollBarAlwaysOn : Qt.ScrollBarAlwaysOff
    verticalScrollBarPolicy: contentItem.height > scroll.height ? Qt.ScrollBarAlwaysOn : Qt.ScrollBarAlwaysOff
}
