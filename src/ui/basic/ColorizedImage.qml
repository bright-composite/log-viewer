import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    id: item
    property string source
    property color color

    Image {
        id: image
        anchors.fill: parent
        source: item.source

        sourceSize {
            width: image.width
            height: image.height
        }

        visible: false
    }

    ColorOverlay {
        anchors.fill: parent
        source: image
        color: item.color
    }
}
