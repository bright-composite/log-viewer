import QtQuick 2.0

QtObject {
    property bool left: true
    property bool top: true
    property bool right: true
    property bool bottom: true

    property bool leftTop: left || top
    property bool rightTop: right || top
    property bool leftBottom: left || bottom
    property bool rightBottom: right || bottom
}
