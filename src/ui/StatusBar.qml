import QtQuick 2.0
import QtQuick.Controls 1.4 as Controls
import QtQuick.Controls.Styles 1.4

import "."

Rectangle {
    id: statusBar

    anchors {
        bottom: parent.bottom
        left: parent.left
        right: parent.right
    }

    height: Style.statusBar.height
    color: Style.statusBar.color

    property string fileName
    property string adapterName
    property int recordCount: 0
    property int lineCount: 0
    property real progress: 0

    property int intProgress: Math.round(progress * progressPeriod)
    readonly property int progressPeriod: 1000

    signal stop
    signal refresh

    Behavior on intProgress {
        enabled: progress > 1

        NumberAnimation {
            duration: 200
        }
    }

    Rectangle {
        anchors {
            left: parent.left
            top: parent.top
            right: parent.right
        }

        height: 1
        color: "#10ffffff"
    }

    Row {
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
            leftMargin: 10
        }

        spacing: 8

        Label {
            anchors {
                top: parent.top
                bottom: parent.bottom
            }

            text: fileName
            color: Style.color.text.secondary
            verticalAlignment: Qt.AlignVCenter
        }

        Label {
            anchors {
                top: parent.top
                bottom: parent.bottom
            }

            text: "Записей: " + statusBar.recordCount
            color: Style.color.text.secondary
            verticalAlignment: Qt.AlignVCenter
        }

        Label {
            anchors {
                top: parent.top
                bottom: parent.bottom
            }

            text: "Строк: " + statusBar.lineCount
            color: Style.color.text.secondary
            verticalAlignment: Qt.AlignVCenter
        }

        ImageButton {
            id: refresh

            anchors {
                top: parent.top
                bottom: parent.bottom
                margins: 4
            }

            source: "qrc:/icons/refresh.png"
            visible: intProgress == progressPeriod
            iconScale: 1.8
            hasBackground: false

            width: height
            tooltipText: "Перезагрузить файл"

            onClicked: {
                statusBar.refresh()
            }
        }
    }

    Row {
        anchors {
            top: parent.top
            bottom: parent.bottom
            right: parent.right
            rightMargin: 10
        }

        spacing: 8

        BusyIndicator {
            anchors {
                top: parent.top
                bottom: parent.bottom
                margins: 4
            }

            width: height
            active: intProgress < progressPeriod
            color: Style.color.text.primary
            visible: intProgress < progressPeriod
        }

        Controls.ProgressBar {
            id: progressBar

            anchors {
                top: parent.top
                bottom: parent.bottom
                margins: 4
            }

            style: ProgressBarStyle {
                background: Rectangle {
                    color: Style.color.accent
                    border.color: Style.color.selected
                    border.width: 1
                    radius: 0

                    implicitWidth: 200
                    implicitHeight: 24
                }

                progress: Rectangle {
                    color: Style.color.selected
                    border.color: Style.color.selected
                    radius: 0
                }
            }

            width: 100

            minimumValue: 0.0
            maximumValue: 1.0
            value: intProgress / progressPeriod

            visible: intProgress < progressPeriod

            Item {
                anchors {
                    top: parent.top
                    bottom: parent.bottom
                    left: parent.left
                }

                width: parent.width * intProgress / progressPeriod
                clip: true

                Label {
                    anchors {
                        top: parent.top
                        bottom: parent.bottom
                        left: parent.left
                    }

                    width: progressBar.width

                    text: (intProgress / 10) + "%"
                    color: Style.color.text.selected
                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                }
            }

            Item {
                anchors {
                    top: parent.top
                    bottom: parent.bottom
                    right: parent.right
                }

                width: parent.width * (1 - intProgress / progressPeriod)
                clip: true

                Label {
                    anchors {
                        top: parent.top
                        bottom: parent.bottom
                        right: parent.right
                    }

                    width: progressBar.width

                    text: (intProgress / 10) + "%"
                    color: Style.color.text.secondary
                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                }
            }
        }

        Rectangle {
            anchors {
                top: parent.top
                bottom: parent.bottom
                margins: 6
            }

            width: height
            color: stopArea.containsPress ? Style.color.text.accent : stopArea.containsMouse ? Style.color.text.secondary : Style.color.text.primary

            visible: intProgress < progressPeriod

            border {
                width: 1
                color: Style.color.selected
            }

            Behavior on color {
                ColorAnimation {
                    duration: 100
                }
            }

            TooltipArea {
                id: stopArea

                anchors.fill: parent

                text: "Остановить операцию"
                cursorShape: Qt.PointingHandCursor

                onClicked: {
                    statusBar.stop()
                }
            }
        }

        Label {
            anchors {
                top: parent.top
                bottom: parent.bottom
            }

            text: adapterName
            color: Style.color.text.secondary
            verticalAlignment: Qt.AlignVCenter
        }
    }
}
