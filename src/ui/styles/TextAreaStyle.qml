import QtQuick 2.0
import QtQuick.Controls.Styles 1.4 as Styles

import ui 1.0

Styles.TextAreaStyle {
    padding {
        left: 0
        bottom: 0
        right: 0
    }

    transientScrollBars: false

    font {
        pointSize: Style.font.size.primary
        family: Style.font.mono.name
    }

    frame: Rectangle {
        anchors {
            fill: parent
            margins: -1
        }

        color: Style.color.accent
        radius: Style.input.radius

        border {
            width: 1
            color: Style.color.border.accent
        }
    }

    textColor: Style.color.text.primary
    selectionColor: Style.color.selected
    selectedTextColor: Style.color.text.selected

    handle: Style.scrollbar.handle
    decrementControl: Style.scrollbar.decrementControl
    incrementControl: Style.scrollbar.incrementControl
    corner: Style.scrollbar.corner
    scrollBarBackground: Style.scrollbar.scrollBarBackground
}
