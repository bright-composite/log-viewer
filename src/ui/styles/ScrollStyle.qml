import QtQuick 2.0
import QtQuick.Controls.Styles 1.4

import ui 1.0

ScrollViewStyle {
    padding {
        left: 0
        bottom: 0
        right: 0
    }

    transientScrollBars: false

    handle: Style.scrollbar.handle
    decrementControl: Style.scrollbar.decrementControl
    incrementControl: Style.scrollbar.incrementControl
    corner: Style.scrollbar.corner
    scrollBarBackground: Style.scrollbar.scrollBarBackground
}
