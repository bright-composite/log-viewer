pragma Singleton

import QtQuick 2.0
import ui 1.0

QtObject {
    id: style

    property string colorScheme: "light"

    readonly property QtObject currentScheme: style.hasOwnProperty(colorScheme) ? style[colorScheme] : style.light

    readonly property QtObject color: currentScheme.color

    readonly property QtObject header: QtObject {
        readonly property int height: 84
        readonly property int heightMin: 48
        readonly property color color: style.color.primary
    }

    readonly property QtObject statusBar: QtObject {
        readonly property int height: 24
        readonly property color color: style.color.primary
    }

    readonly property QtObject font: QtObject {
        readonly property FontLoader primary: FontLoader {
            source: "qrc:/fonts/Tahoma.ttf"
        }

        readonly property FontLoader mono: FontLoader {
            source: "qrc:/fonts/VeraMono.ttf"
        }

        readonly property QtObject size: QtObject {
            readonly property int primary: 10
            readonly property int small: 8
        }
    }

    readonly property QtObject button: QtObject {
        readonly property int radius: 0
    }

    readonly property QtObject input: QtObject {
        readonly property int radius: 0
    }

    readonly property QtObject scrollbar: QtObject {
        id: scrollbar

        readonly property int width: 10
        readonly property bool showControls: true

        function controlsColor(styleData, base) {
            return styleData.pressed ? Qt.tint(base, style.color.darker) : styleData.hovered ? Qt.tint(base, style.color.lighter) : base
        }

        readonly property Component handle: Item {
            implicitWidth: scrollbar.width
            implicitHeight: scrollbar.width

            Rectangle {
                anchors {
                    fill: parent
                    leftMargin: styleData.horizontal ? 1 : 0
                    topMargin: styleData.horizontal ? 0 : 0
                    rightMargin: styleData.horizontal ? 1 : -1
                    bottomMargin: styleData.horizontal ? -1 : 2
                }

                color: scrollbar.controlsColor(styleData, style.color.scroll)
                border {
                    width: 1
                    color: style.color.darker
                }

                Behavior on color {
                    enabled: Config.enableAnimations

                    ColorAnimation {
                        duration: 100
                    }
                }
            }
        }

        readonly property Component decrementControl: Item {
            implicitWidth: scrollbar.showControls ? scrollbar.width : 0
            implicitHeight: scrollbar.showControls ? scrollbar.width : 0

            visible: scrollbar.showControls
            clip: true

            Rectangle {
                anchors {
                    fill: parent
                    leftMargin: styleData.horizontal ? 0 : 1
                    topMargin: styleData.horizontal ? 1 : 0
                }

                color: scrollbar.controlsColor(styleData, style.color.accent)

                ColorizedImage {
                    anchors.centerIn: parent
                    width: scrollbar.width / 2
                    height: scrollbar.width / 2
                    source: "qrc:/icons/caret-arrow-small.png"
                    color: "#333"

                    rotation: styleData.horizontal ? 0 : 90
                }

                Borders {}
            }
        }

        readonly property Component incrementControl: Item {
            implicitWidth: scrollbar.showControls ? scrollbar.width : 0
            implicitHeight: scrollbar.showControls ? scrollbar.width : 0

            visible: scrollbar.showControls
            clip: true

            Rectangle {
                anchors {
                    fill: parent
                    leftMargin: styleData.horizontal ? 0 : 1
                    topMargin: styleData.horizontal ? 1 : 0
                }

                color: scrollbar.controlsColor(styleData, style.color.accent)

                ColorizedImage {
                    anchors.centerIn: parent
                    width: scrollbar.width / 2
                    height: scrollbar.width / 2
                    source: "qrc:/icons/caret-arrow-small.png"
                    color: "#333"

                    rotation: styleData.horizontal ? 180 : -90
                }

                Borders {}
            }
        }

        readonly property Component corner: Rectangle {
            implicitWidth: scrollbar.width - 1
            implicitHeight: scrollbar.width - 1
            color: style.color.secondary
        }

        readonly property Component scrollBarBackground: Rectangle {
            implicitWidth: scrollbar.width
            implicitHeight: scrollbar.width
            color: style.color.alternate
        }
    }

    readonly property QtObject light: QtObject {
        readonly property QtObject color: QtObject {
            readonly property color primary: "#e7e7e7"
            readonly property color secondary: "#f7f7f7"
            readonly property color alternate: "#f0f0f0"
            readonly property color accent:  "#ffffff"
            readonly property color input:  "#ffffff"
            readonly property color selected: "#a0a0a0"
            readonly property color highlight: "#a0ffffff"

            readonly property color error: "#f54"
            readonly property color warning: "#fa0"
            readonly property color notice: "#4af"

            readonly property color scroll:  "#c0c0c0"
            readonly property color lighter: "#20ffffff"
            readonly property color darker:  "#20000000"

            readonly property QtObject text: QtObject {
                readonly property color primary: "#444"
                readonly property color secondary: "#666"
                readonly property color highlight: "#a0ffee99"
                readonly property color light: "#bbb"
                readonly property color accent: "#111"
                readonly property color selected: "#fff"
                readonly property color error: "#a10"
                readonly property color warning: "#a50"
                readonly property color notice: "#04a"
            }

            readonly property QtObject border: QtObject {
                readonly property color primary: "#ddd"
                readonly property color accent: "#ccc"
                readonly property color light: "#70dddddd"
            }

            readonly property QtObject button: QtObject {
                readonly property color primary: "#f7f7f7"
                readonly property color pressed: "#d0d0d0"
                readonly property color hovered: "#fff"
                readonly property color selected: style.color.selected

                readonly property QtObject border: QtObject {
                    readonly property color primary: "#ddd"
                    readonly property color selected: "#ddd"
                }
            }
        }
    }

    readonly property QtObject dark: QtObject {
        readonly property QtObject color: QtObject {
            readonly property color primary: "#303030"
            readonly property color secondary: "#272727"
            readonly property color alternate: "#212121"
            readonly property color accent:  "#555"
            readonly property color input:  "#404040"
            readonly property color selected: "#a0a0a0"
            readonly property color highlight: "#40ffffff"

            readonly property color error: "#f54"
            readonly property color warning: "#fa0"
            readonly property color notice: "#4af"

            readonly property color scroll:  "#404040"
            readonly property color lighter: "#10ffffff"
            readonly property color darker:  "#10000000"

            readonly property QtObject text: QtObject {
                readonly property color primary: "#ccc"
                readonly property color secondary: "#888"
                readonly property color highlight: "#d0227700"
                readonly property color light: "#70bbbbbb"
                readonly property color accent: "#fff"
                readonly property color selected: "#111"
                readonly property color info: "#ccf"
                readonly property color error: "#fcc"
                readonly property color warning: "#ffc"
                readonly property color notice: "#6cf"
            }

            readonly property QtObject border: QtObject {
                readonly property color primary: "#272727"
                readonly property color accent: "#111"
                readonly property color light: "#70272727"
            }

            readonly property QtObject button: QtObject {
                readonly property color primary: "#606060"
                readonly property color pressed: "#404040"
                readonly property color hovered: "#676767"
                readonly property color selected: style.color.selected

                readonly property QtObject border: QtObject {
                    readonly property color primary: "#272727"
                    readonly property color selected: "#222"
                }
            }
        }
    }
}
