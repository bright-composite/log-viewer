import QtQuick 2.5

import "."

Rectangle {
    id: header

    anchors {
        top: parent.top
        left: parent.left
        right: parent.right
    }

    property bool searchEnabled: true
    property bool filteringEnabled: true
    property bool filtered: false
    property bool searched: false
    property int occurrenciesCount: 0

    property alias searchInput: searchInput

    property bool minimized: false

    height: minimized ? Style.header.heightMin : Style.header.height
    color: Style.header.color

    z: 50

    Behavior on height {
        enabled: Config.enableAnimations

        NumberAnimation {
            duration: 200

            easing {
                type: Easing.OutQuad
            }
        }
    }

    Rectangle {
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        height: 1

        color: Style.color.border.accent
    }

    Row {
        id: leftRow

        anchors {
            verticalCenter: parent.verticalCenter
            left: parent.left
            leftMargin: 15
        }

        height: header.height - 12

        spacing: 8

        ActionButton {
            width: Math.floor(parent.height * 1.1)
            height: parent.height

            source: "qrc:/icons/folder.png"
            text: "Открыть"
            minimized: header.minimized

            tooltipText: "Открыть файл"

            onClicked: {
                root.openFile()
            }
        }
    }

    Item {
        id: center

        anchors {
            verticalCenter: parent.verticalCenter
            left: leftRow.right
            right: rightRow.left
        }

        height: header.height - 12

        Row {
            anchors.right: parent.right
            height: parent.height

            spacing: 8

            Item {
                width: childrenRect.width
                height: 36
                visible: searched

                Row {
                    height: parent.height

                    spacing: 8

                    Label {
                        anchors.verticalCenter: parent.verticalCenter
                        text: "Найдено вхождений: " + occurrenciesCount
                    }

                    ColorizedImage {
                        anchors.verticalCenter: parent.verticalCenter
                        width: 12
                        height: 12

                        source: "qrc:/icons/close.png"
                        color: occurrenciesArea.containsPress ? Style.color.text.secondary : occurrenciesArea.containsMouse ? Style.color.text.accent : Style.color.text.primary

                        Behavior on color {
                            enabled: Config.enableAnimations

                            ColorAnimation {
                                duration: 100
                            }
                        }
                    }
                }


                TooltipArea {
                    id: occurrenciesArea
                    anchors.fill: parent

                    text: "Сбросить результаты поиска (Ctrl+Shift+F)"

                    cursorShape: Qt.PointingHandCursor

                    onClicked: {
                        root.resetSearch()
                    }
                }
            }

            SearchInput {
                id: searchInput

                width: Math.min(200, center.width - 4)
                height: parent.height
                visible: searchEnabled

                onSearch: {
                    root.search(text, regexp, caseSensitive)
                }
            }
        }
    }

    Row {
        id: rightRow

        anchors {
            verticalCenter: parent.verticalCenter
            right: parent.right
            rightMargin: 15
        }

        height: header.height - 12

        Item {
            width: parent.height * 1.1 + 4
            height: parent.height

            ActionButton {
                anchors.right: parent.right

                width: Math.floor(parent.height * 1.1)
                height: parent.height

                visible: filteringEnabled
                source: "qrc:/icons/filter.png"
                text: "Фильтр"
                minimized: header.minimized
                iconScale: 0.8

                tooltipText: "Фильтр (Ctrl+G)"

                onClicked: {
                    root.filter()
                }
            }
        }

        Item {
            width: (parent.height * 1.1 + 4) * intProgress / 100
            height: parent.height

            property int intProgress: filtered ? 100 : 0

            visible: intProgress > 0
            opacity: Math.max(0, (intProgress - 20) / 80)

            Behavior on intProgress {
                enabled: Config.enableAnimations

                NumberAnimation {
                    duration: 200
                    easing {
                        type: Easing.OutQuad
                    }
                }
            }

            ActionButton {
                id: reset
                anchors.right: parent.right

                width: parent.height * 1.1
                height: parent.height

                source: "qrc:/icons/filter.png"
                text: "Сбросить"
                minimized: header.minimized
                iconScale: 0.8

                tooltipText: "Сбросить фильтр (Ctrl+Shift+G)"

                onClicked: {
                    root.resetFilter()
                }

                ColorizedImage {
                    x: reset.image.x + reset.image.width * 0.9 - width
                    y: reset.image.y + reset.image.height * 0.9 - height

                    width: reset.image.width / 4
                    height: reset.image.width / 4
                    source: "qrc:/icons/close.png"
                    color: Style.color.text.error
                }
            }
        }

        Item {
            width: (parent.height * 0.5 + 4)
            height: parent.height

            Column {
                anchors.right: parent.right
                width: parent.height * 0.5 - 2
                spacing: 4

                ImageButton {
                    width: parent.width
                    height: parent.width

                    iconScale: 0.7

                    source: "qrc:/icons/settings.png"
                    tooltipText: "Настройки (Ctrl+O)"

                    onClicked: {
                        root.settings()
                    }
                }

                ImageButton {
                    width: parent.width
                    height: parent.width

                    iconScale: 0.6

                    source: "qrc:/icons/caret-arrow-small.png"
                    tooltipText: minimized ? "Развернуть" : "Свернуть"
                    rotation: minimized ? -90 : 90

                    Behavior on rotation {
                        enabled: Config.enableAnimations

                        RotationAnimation {
                            duration: 200
                        }
                    }

                    onClicked: {
                        minimized = !minimized
                    }
                }
            }
        }
    }
}
