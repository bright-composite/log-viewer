import QtQuick 2.0
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0

import test.logs 1.0
import ui 1.0

Item {
    id: adapter

    property Table table
    property EventListModel model
    property Main root

    property var layout: [{
        role: "line",
        title: "",
        minimumWidth: 32,
        horizontalAlignment: Qt.AlignRight,
        elideMode: Qt.ElideRight,
        delegate: lineDelegate
    }, {
        role: "severity",
        title: "",
        width: 32,
        minimumWidth: 32,
        delegate: imageDelegate,
        resizable: false
    }, {
        role: "time",
        title: "Время",
        minimumWidth: 140,
        horizontalAlignment: Qt.AlignHCenter,
        delegate: timeDelegate
    }, {
        role: "message",
        title: "Сообщение",
        minimumWidth: 200,
        delegate: messageDelegate
    }]

    property Component lineDelegate: Item {
        anchors.fill: parent

        Label {
            anchors.fill: parent
            anchors.margins: 5

            text: {
                if(!styleData.value) {
                    return "-"
                }

                if(styleData.value >= 1000) {
                    if(styleData.value >= 1000000) {
                        if(width < (("" + styleData.value).length - 3) * 8) {
                            return Math.floor(styleData.value / 1000000) + "kk"
                        }
                    }

                    if(width < ("" + styleData.value).length * 8) {
                        return Math.floor(styleData.value / 1000) + "k"
                    }
                }

                return styleData.value
            }

            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
            elide: styleData.elideMode
            color: Style.color.text.secondary
            font.pointSize: 9
        }
    }

    property Component imageDelegate: Item {
        width: staticRowHeight - 10
        height: staticRowHeight - 10

        ColorizedImage {
            anchors.centerIn: parent
            width: staticRowHeight - 10
            height: staticRowHeight - 10

            source: {
                if(model) {
                    switch(model.severity) {
                        case "error":
                            return "qrc:/icons/error.png"
                        case "warning":
                            return "qrc:/icons/warning.png"
                        default:
                            return "qrc:/icons/info.png"
                    }
                }

                return ""
            }

            color: {
                if(model) {
                    switch(model.severity) {
                        case "error":
                            return Style.color.error
                        case "warning":
                            return Style.color.warning
                        case "notice":
                            return Style.color.notice
                        default:
                            return Style.color.primary
                    }
                }

                return Style.color.primary
            }
        }
    }

    property Component timeDelegate: Item {
        anchors.fill: parent

        SelectableText {
            anchors.fill: parent
            anchors.margins: 5

            highlights: adapter.table.findOccurrencies(adapter.table.searchOccurrencies, adapter.model.rowId(styleData.row), "time")

            content: (new Date(styleData.value)).toLocaleString(Qt.locale(), "yyyy/MM/dd HH:mm:ss") || "---"

            font {
                family: Style.font.primary.name
            }

            onSelected: {
                table.selection.clear()
                table.selection.select(styleData.row, styleData.row)
            }

            horizontalAlignment: styleData.textAlignment
            verticalAlignment: Qt.AlignVCenter
            color: styleData.selected ? Style.color.text.accent : Style.color.text.primary
            elide: styleData.elideMode
            wrapMode: styleData.selected ? Text.WordWrap : Text.NoWrap
            clip: true
        }
    }

    property Component messageDelegate: Item {
        height: staticRowHeight

        SelectableText {
            anchors.fill: parent
            anchors.margins: 0
            clip: true

            highlights: adapter.table.findOccurrencies(adapter.table.searchOccurrencies, adapter.model.rowId(styleData.row), filterRole)

            content: styleData.value ? styleData.value : ""
            filterRole: "message"

            horizontalAlignment: styleData.textAlignment
            verticalAlignment: Qt.AlignVCenter

            wrapMode: Text.NoWrap

            onSelected: {
                table.selection.clear()
                table.selection.select(styleData.row, styleData.row)
            }

            color: {
                if(model) {
                    switch(model.severity) {
                        case "error":
                            return Style.color.text.error
                        case "warning":
                            return Style.color.text.warning
                        case "notice":
                            return Style.color.text.notice
                        default:
                            return Style.color.text.accent
                    }
                }

                return Style.color.text.accent
            }

            font {
                family: Style.font.mono.name
                pointSize: Style.font.size.small
            }
        }
    }

    property Component rowDelegate: Rectangle {
        property color baseColor: {
            if(model) {
                switch(model.severity) {
                    case "error":
                        return Style.color.error
                    case "warning":
                        return Style.color.warning
                    case "notice":
                        return Style.color.notice
                    default:
                        return Style.color.primary
                }
            }

            return Style.color.primary
        }


        color: Qt.tint(styleData.selected ? Style.color.accent : (styleData.alternate ? Style.color.alternate : Style.color.secondary), Qt.rgba(baseColor.r, baseColor.g, baseColor.b, 0.02))
        height: staticRowHeight

        border {
            width: styleData.selected ? 1 : 0
            color: Style.color.border.accent
        }
    }

    property Component commonMetrics: Text {
        visible: false

        horizontalAlignment: Qt.AlignLeft
        verticalAlignment: Qt.AlignVCenter

        wrapMode: Text.WordWrap
        maximumLineCount: 1

        font {
            family: Style.font.primary.name
            pointSize: Style.font.size.primary
        }
    }

    readonly property int staticRowHeight: {
        var t = commonMetrics.createObject(adapter, {text: "", width: 100})
        var height = t.height
        t.destroy()

        return height + 10
    }
}
