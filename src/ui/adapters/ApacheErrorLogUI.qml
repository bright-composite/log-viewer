import QtQuick 2.4
import QtGraphicalEffects 1.0

import ui 1.0

GenericAdapterUI {
    id: adapter

    layout: [{
        role: "line",
        title: "",
        minimumWidth: 48,
        horizontalAlignment: Qt.AlignRight,
        elideMode: Qt.ElideRight,
        delegate: lineDelegate
    }, {
        role: "severity",
        title: "",
        width: 32,
        minimumWidth: 32,
        delegate: imageDelegate,
        resizable: false
    }, {
        role: "time",
        title: "Время",
        minimumWidth: 140,
        horizontalAlignment: Qt.AlignHCenter,
        delegate: timeDelegate
    }, {
        role: "unit",
        title: "Модуль",
        horizontalAlignment: Qt.AlignHCenter,
        width: 100,
        minimumWidth: 64,
        delegate: unitDelegate
    }, {
        role: "level",
        title: "Уровень",
        horizontalAlignment: Qt.AlignHCenter,
        width: 100,
        minimumWidth: 64
    }, {
        role: "message",
        title: "Сообщение",
        minimumWidth: 200,
        delegate: messageDelegate
    }]

    property Component unitDelegate: Item {
        anchors.fill: parent

        SelectableText {
            anchors.fill: parent
            anchors.margins: 5

            content: styleData.value || "---"
            filterRole: "unit"

            highlights: adapter.table.findOccurrencies(adapter.table.searchOccurrencies, adapter.model.rowId(styleData.row), filterRole)

            font {
                family: Style.font.primary.name
            }

            onSelected: {
                table.selection.clear()
                table.selection.select(styleData.row, styleData.row)
            }

            horizontalAlignment: styleData.textAlignment
            verticalAlignment: Qt.AlignVCenter
            color: styleData.selected ? Style.color.text.accent : Style.color.text.primary
            elide: styleData.elideMode
            wrapMode: styleData.selected ? Text.WordWrap : Text.NoWrap
        }
    }
}
