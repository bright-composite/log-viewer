import QtQuick 2.0
import QtQuick.Layouts 1.1

import ui 1.0
import models 1.0

Dialog {
    id: dialog

    minimumWidth: 280
    implicitWidth: 360

    modal: true

    content: Item {
        width: dialog.width
        height: content.height + 32

        Column {
            id: content

            anchors {
                top: parent.top
                horizontalCenter: parent.horizontalCenter
                margins: 16
            }

            width: options.width

            spacing: 8

            Column {
                id: options

                width: childrenRect.width

                spacing: 8

                RowLayout {
                    spacing: 8
                    width: 240
                    height: 32

                    Label {
                        Layout.preferredWidth: 120
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignTop

                        verticalAlignment: Qt.AlignVCenter
                        text: "Цветовая схема"

                        color: Style.color.text.accent
                        font.pointSize: 12
                        elide: Qt.ElideRight
                    }

                    Listbox {
                        id: colorScheme
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignVCenter

                        model: [{
                            text: "Светлая",
                            value: "light"
                        }, {
                            text: "Темная",
                            value: "dark"
                        }]

                        PropertyConnection {
                            target: Style
                            targetProperty: "colorScheme"
                            source: colorScheme
                            sourceProperty: "currentValue"
                        }

                        Component.onCompleted: {
                            currentValue = Style.colorScheme
                        }
                    }
                }

                Checkbox {
                    id: animation
                    text: "Включить анимацию"
                    checked: Config.enableAnimations

                    PropertyConnection {
                        target: Config
                        targetProperty: "enableAnimations"
                        source: animation
                        sourceProperty: "checked"
                    }
                }
            }

            Item { width: 1; height: 1 }

            TextButton {
                anchors.horizontalCenter: parent.horizontalCenter
                height: 32

                text: "Закрыть"

                onClicked: {
                    close()
                }
            }
        }
    }

    Component.onCompleted: {
        x = (root.width - width) / 2
        y = root.header.height
    }
}
