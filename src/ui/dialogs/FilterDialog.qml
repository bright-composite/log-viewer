import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Window 2.0

import test.logs 1.0
import ui 1.0
import models 1.0

Dialog {
    id: dialog

    minimumWidth: 430
    implicitWidth: 430
    title: "Фильтр"

    property TableView table
    property EventListModel model

    readonly property var defaultFields: ["line", "severity", "time", "message"]
    property var customFields: []
    property var customFilters: ({})

    property int reflowLimit: 720

    signal apply(var filter, date startTime, date endTime)
    signal reset

    modal: true

    content: Item {
        width: dialog.width
        height: content.height + 64

        Column {
            id: content

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                margins: 32
            }

            spacing: 32

            Grid {
                id: grid

                anchors {
                    left: parent.left
                    right: parent.right
                }

                columns: dialog.width > reflowLimit ? 2 : 1
                rowSpacing: 16
                columnSpacing: 16

                Item {
                    width: dialog.width > reflowLimit ? (content.width - 16) / 2 : content.width
                    height: general.height

                    Column {
                        id: general

                        anchors {
                            left: parent.left
                            right: parent.right
                        }

                        spacing: 8

                        RowLayout {
                            id: dateFromLayout

                            anchors {
                                left: parent.left
                                right: parent.right
                            }

                            spacing: 8
                            height: 32

                            Label {
                                text: "Начало:"

                                Layout.preferredWidth: dialog.width > reflowLimit ? 60 : 100
                                Layout.fillHeight: true
                                Layout.alignment: Qt.AlignTop

                                verticalAlignment: Qt.AlignVCenter

                                color: Style.color.text.accent
                                font.pointSize: 12
                                elide: Qt.ElideRight
                            }

                            DateTimeInput {
                                id: dateFrom
                                Layout.fillWidth: true
                                Layout.fillHeight: true

                                minimumDate: dialog.model.defaultStartTime
                                maximumDate: dialog.model.defaultEndTime
                                dateTime: dialog.model.defaultStartTime
                            }
                        }

                        RowLayout {
                            id: dateToLayout

                            anchors {
                                left: parent.left
                                right: parent.right
                            }

                            spacing: 8
                            height: 32

                            Label {
                                text: "Конец:"

                                Layout.preferredWidth: dialog.width > reflowLimit ? 60 : 100
                                Layout.fillHeight: true
                                Layout.alignment: Qt.AlignTop

                                verticalAlignment: Qt.AlignVCenter

                                color: Style.color.text.accent
                                font.pointSize: 12
                                elide: Qt.ElideRight
                            }

                            DateTimeInput {
                                id: dateTo
                                Layout.fillWidth: true
                                Layout.fillHeight: true

                                minimumDate: dialog.model.defaultStartTime
                                maximumDate: dialog.model.defaultEndTime
                                dateTime: dialog.model.defaultEndTime
                            }
                        }
                    }
                }

                Item {
                    width: dialog.width > reflowLimit ? (content.width - 16) / 2 : content.width
                    height: extra.height

                    Column {
                        id: extra

                        anchors {
                            left: parent.left
                            right: parent.right
                        }

                        spacing: 8

                        Repeater {
                            id: repeater
                            model: customFields

                            RowLayout {
                                anchors {
                                    left: parent.left
                                    right: parent.right
                                }

                                spacing: 8
                                height: 32

                                Label {
                                    Layout.preferredWidth: 100
                                    Layout.fillHeight: true
                                    Layout.alignment: Qt.AlignTop

                                    verticalAlignment: Qt.AlignVCenter
                                    text: modelData.title + ":"

                                    color: Style.color.text.accent
                                    font.pointSize: 12
                                    elide: Qt.ElideRight
                                }

                                Item {
                                    id: input
                                    Layout.fillWidth: true
                                    Layout.fillHeight: true

                                    property string text: ""
                                    property bool regex: false

                                    property var defaults: dialog.model.getDefaults(modelData.role)
                                    property bool hasDefaults: defaults.length > 0

                                    Component {
                                        id: regexScheme

                                        RegexInput {
                                            anchors.fill: parent
                                            field.font.pointSize: 12

                                            PropertyConnection {
                                                target: input
                                                source: parent
                                                property: "text"
                                            }

                                            PropertyConnection {
                                                target: input
                                                source: parent
                                                property: "regex"
                                            }
                                        }
                                    }

                                    Component {
                                        id: selectScheme

                                        Listbox {
                                            id: listbox
                                            anchors.fill: parent
                                            allowEmpty: true

                                            model: input.defaults.map(function(e) {
                                                return { text: e, value: e }
                                            })

                                            PropertyConnection {
                                                target: input
                                                source: parent
                                                targetProperty: "text"
                                                sourceProperty: "currentValue"

                                                function map() {
                                                    // string can not be null
                                                    return listbox.currentValue !== null ? listbox.currentValue : ""
                                                }
                                            }

                                            Binding {
                                                target: input
                                                property: "regex"
                                                value: false
                                            }
                                        }
                                    }

                                    Loader {
                                        id: loader
                                        anchors.fill: parent
                                        sourceComponent: input.hasDefaults ? selectScheme : regexScheme
                                    }
                                }

                                Component.onCompleted: {
                                    customFilters[modelData.role] = input
                                }
                            }
                        }

                        RowLayout {
                            anchors {
                                left: parent.left
                                right: parent.right
                            }

                            spacing: 8
                            height: 32

                            Label {
                                Layout.preferredWidth: 100
                                Layout.fillHeight: true
                                Layout.alignment: Qt.AlignTop

                                verticalAlignment: Qt.AlignVCenter
                                text: "Сообщение:"

                                color: Style.color.text.accent
                                font.pointSize: 12
                                elide: Qt.ElideRight
                            }

                            RegexInput {
                                id: message

                                Layout.fillWidth: true
                                Layout.alignment: Qt.AlignVCenter

                                field.font.pointSize: 12
                            }
                        }
                    }
                }
            }

            Item {
                anchors {
                    left: parent.left
                    right: parent.right
                }

                height: 32

                RowLayout {
                    anchors.fill: parent

                    spacing: 16

                    TextButton {
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        text: "Применить"

                        onClicked: {
                            var hasSomeFields = false

                            var filter = Object.keys(customFilters).reduce(function(acc, field) {
                                if(customFilters[field].text) {
                                    acc[field] = { expr: customFilters[field].text, syntax: customFilters[field].regex ? EventListModel.RegExp2 : EventListModel.FixedString }
                                    hasSomeFields = true
                                }

                                return acc
                            }, {})

                            if(message.text) {
                                filter["message"] = { expr: message.text, syntax: message.regex ? EventListModel.RegExp2 : EventListModel.FixedString }
                                hasSomeFields = true
                            }

                            if(dateFrom.text) {
                                hasSomeFields = true
                            }

                            if(dateTo.text) {
                                hasSomeFields = true
                            }

                            if(hasSomeFields) {
                                apply(filter, dateFrom.text ? dateFrom.dateTime : null, dateTo.text ? dateTo.dateTime : null)
                            } else {
                                reset()
                            }
                        }
                    }

                    TextButton {
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        text: "Сбросить"

                        onClicked: {
                            reset()
                        }
                    }

                    TextButton {
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        text: "Закрыть"

                        onClicked: {
                            close()
                        }
                    }
                }
            }
        }
    }

    function updateFields() {
        customFields = []
        var fields = []

        for(var i = 0; i < table.columnCount; ++i) {
            var column = table.getColumn(i)

            if(defaultFields.indexOf(column.role) == -1) {
                fields.push(column)
            }
        }

        customFields = fields
    }

    Connections {
        target: model

        onAdapterChanged: {
            updateFields()
        }

        onDefaultsUpdated: {
            updateFields()
        }

        onSourceLoaded: {
            dateFrom.setDateTime(model.defaultStartTime)
            dateTo.setDateTime(model.defaultEndTime)
        }
    }

    Connections {
        target: root

        onResetFilter: {
            dateFrom.setDateTime(model.defaultStartTime)
            dateTo.setDateTime(model.defaultEndTime)

            Object.keys(customFilters).forEach(function(field) {
                customFilters[field].text = ""
                customFilters[field].regex = false
            }, {})

            message.text = ""
            message.regex = false
        }
    }

    Component.onCompleted: {
        updateFields()

        x = (root.width - width) / 2
        y = root.header.height
    }
}
