import QtQuick 2.0

import ui 1.0
import models 1.0

Popup {
    id: popup

    property var selectedTime: DateTime.time()
    property bool focused: scope.activeFocus

    resize {
        left: false
        top: false
        right: false
        bottom: false
    }

    FocusScope {
        id: scope
        anchors.fill: parent

        TimePicker {
            id: picker

            anchors.fill: parent

            property PropertyConnection connection: PropertyConnection {
                target: popup
                source: picker
                property: "selectedTime"
            }
        }
    }
}
