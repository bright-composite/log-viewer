import QtQuick 2.0
import QtGraphicalEffects 1.0

import ui 1.0

Resizable {
    id: popup

    property real progress: 0
    property bool shown: false

    parent: dialog && dialog != popup ? dialog : root.contentItem // seek for a dialog in the hierarchy

    visible: opacity > 0
    opacity: popup.progress / 100

    function getPosition(element) {
        var parent = element.parent
        var x = element.x, y = element.y

        while(parent != popup.parent) {
            if(!parent) {
                return {x: 0, y: 0}
            }

            x += parent.x
            y += parent.y
            parent = parent.parent
        }

        return {x: x, y: y}
    }

    DropShadow {
        anchors.fill: background
        source: background
        horizontalOffset: 2
        verticalOffset: 2

        color: "#30000000"
    }

    Rectangle {
        id: background
        anchors.fill: parent

        color: Style.color.accent

        border {
            width: 1
            color: Style.color.border.primary
        }
    }

    Behavior on progress {
        enabled: Config.enableAnimations

        SmoothedAnimation {
            duration: 200
        }
    }

    function open() {
        progress = 100
    }

    function close() {
        progress = 0
    }

    MouseArea {
        id: area
        anchors.fill: parent
        hoverEnabled: true

        onClicked: {
            forceActiveFocus()
        }
    }

    onShownChanged: {
        if(shown) {
            open()
        } else {
            close()
        }
    }
}
