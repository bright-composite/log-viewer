import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import ui 1.0
import models 1.0

Popup {
    id: popup

    property date minimumDate: calendar.minimumDate
    property date maximumDate: calendar.maximumDate
    property date selectedDate

    resize {
        left: false
        top: false
        right: false
        bottom: false
    }

    DatePicker {
        id: calendar

        anchors.fill: parent
        minimumDate: popup.minimumDate
        maximumDate: popup.maximumDate

        property PropertyConnection connection: PropertyConnection {
            target: popup
            source: calendar
            property: "selectedDate"
        }
    }
}
