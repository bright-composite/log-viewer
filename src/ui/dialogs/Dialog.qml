import QtQuick 2.5

import ui 1.0

Popup {
    id: dialog

    property bool modal: false
    property bool needHeader: true
    property string title
    property Item content

    implicitHeight: wrapper.height + header.height
    minimumWidth: 300
    minimumHeight: wrapper.height + header.height

    width: implicitWidth
    height: implicitHeight

    z: 101

    resize {
        top: false
        bottom: false
    }

    function close() {
        progress = 0
        dialog.parent.forceActiveFocus()
    }

    Rectangle {
        parent: root.contentItem

        z: dialog.z - 1

        anchors.fill: parent
        color: "#80000000"
        opacity: modal ? dialog.progress / 100 : 0
        visible: dialog.visible

        MouseArea {
            anchors.fill: parent
            visible: dialog.visible

            onClicked: {
                forceActiveFocus()
                dialog.close()
                mouse.accepted = false
            }
        }
    }

    Item {
        id: dragger
        parent: root.contentItem

        x: dialog.x
        y: dialog.y
        z: dialog.z + 1

        width: header.width
        height: header.height

        visible: dialog.visible

        MouseArea {
            anchors.fill: parent

            cursorShape: pressed ? Qt.ClosedHandCursor : Qt.OpenHandCursor

            drag {
                target: dialog
                minimumX: 0
                minimumY: 0
                maximumX: root.width - dialog.width
                maximumY: root.height - dialog.height
            }
        }
    }

    Rectangle {
        id: header

        parent: root.contentItem

        x: dialog.x
        y: dialog.y
        z: dialog.z + 2

        width: dialog.width
        height: 28

        visible: dialog.visible && needHeader

        color: Style.header.color

        Rectangle {
            anchors {
                bottom: parent.bottom
                left: parent.left
                right: parent.right
            }

            height: 1

            color: Style.color.border.accent
        }

        Label {
            anchors.fill: parent

            text: title
            color: Style.color.text.accent

            font {
                pointSize: 12
            }

            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
        }

        Item {
            anchors {
                top: parent.top
                right: parent.right
                margins: 2
            }

            width: 24
            height: 24

            z: 103

            ImageButton {
                width: 24
                height: 24

                z: 103

                source: "qrc:/icons/close.png"
                iconScale: 0.6

                tooltipText: "Закрыть"

                onClicked: {
                    close()
                }
            }
        }
    }

    Item {
        id: wrapper

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            topMargin: needHeader ? header.height : 0
        }

        height: content ? content.height : 0
    }

    onContentChanged: {
        if(content) {
            content.parent = wrapper
        }
    }
}
