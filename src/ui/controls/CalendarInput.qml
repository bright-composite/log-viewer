import QtQuick 2.0

import ui 1.0
import models 1.0

Input {
    id: input

    property var date
    property date minimumDate
    property date maximumDate

    validator: RegExpValidator {
        regExp: /\d{2}\/\d{2}\/\d{2}/
    }

    field.font {
        family: Style.font.mono.name
        pointSize: 11
    }

    PropertyConnection {
        targetProperty: "date"

        function map(value) {
            var date = DateTime.dateFromString(value)

            if(value) {
                var dt = DateTime.dateTime(value)

                if(dt > maximumDate) {
                    input.text = DateTime.dateToString(date = DateTime.date(maximumDate))
                }

                if(dt < minimumDate) {
                    input.text = DateTime.dateToString(date = DateTime.date(minimumDate))
                }
            }

            return date
        }

        sourceProperty: "text"

        function unmap(value) {
            return DateTime.dateToString(value)
        }
    }

    onFocusedChanged: {
        if(focused && input.text == "") {
            popup.selectedDate = minimumDate
        }
    }

    textMargin: 1

    appendDelegate: ImageButton {
        anchors {
            verticalCenter: parent.verticalCenter
            right: parent.right
            rightMargin: 4
        }

        width: input.height - 16
        height: input.height - 16

        source: "qrc:/icons/close.png"
        iconScale: 0.5
        visible: input.text != ""
        hasBackground: false

        tooltipText: "Очистить"

        onClicked: {
            input.text = ""
            input.parent.forceActiveFocus()
        }
    }

    CalendarPopup {
        id: popup

        property var position: getPosition(input)

        x: Math.min(position.x + (input.width - width) / 2, parent.width - width - 20)
        y: position.y + input.height
        width: Math.max(input.width, 230)
        height: width

        PropertyConnection {
            target: input
            targetProperty: "date"

            function map(value) {
                return DateTime.date(value)
            }

            source: popup
            sourceProperty: "selectedDate"

            function unmap(value) {
                var dt = DateTime.dateTime(value)

                if(dt > maximumDate) {
                    input.date = DateTime.date(dt = maximumDate)
                }

                if(dt < minimumDate) {
                    input.date = DateTime.date(dt = minimumDate)
                }

                return dt
            }
        }

        shown: input.visible && input.focused

        minimumDate: input.minimumDate
        maximumDate: input.maximumDate
    }
}
