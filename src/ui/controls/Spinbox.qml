import QtQuick 2.0
import Qt.labs.controls 1.0

import ui 1.0

SpinBox {
    id: control

    font {
        family: Style.font.primary.name
        pointSize: Style.font.size.primary
    }

    property bool editable: true

    contentItem: TextInput {
        z: 2
        text: control.textFromValue(control.value, control.locale)

        font: control.font
        color: Style.color.text.primary
        selectionColor: Style.color.primary
        selectedTextColor: Style.color.text.selected
        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter

        readOnly: !control.editable
        validator: control.validator
        inputMethodHints: Qt.ImhFormattedNumbersOnly
    }

    up.indicator: Rectangle {
        x: control.mirrored ? 0 : parent.width - width
        height: parent.height
        implicitWidth: 40
        implicitHeight: 40

        property color baseColor: up.pressed ? Style.color.button.pressed : upArea.containsMouse ? Style.color.button.hovered : Style.color.button.primary

        color: control.enabled ? baseColor : Style.color.button.primary
        border.color: Style.color.button.border.primary

        Label {
            text: "+"
            anchors.fill: parent
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            id: upArea
            hoverEnabled: true
            anchors.fill: parent
        }
    }

    down.indicator: Rectangle {
        x: control.mirrored ? parent.width - width : 0
        height: parent.height
        implicitWidth: 40
        implicitHeight: 40

        property color baseColor: down.pressed ? Style.color.button.pressed : downArea.containsMouse ? Style.color.button.hovered : Style.color.button.primary

        color: control.enabled ? baseColor : Style.color.button.primary
        border.color: Style.color.button.border.primary

        Label {
            text: "-"
            anchors.fill: parent
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            id: downArea
            hoverEnabled: true
            anchors.fill: parent
        }
    }

    background: Rectangle {
        implicitWidth: 140

        color: Style.color.input
        radius: Style.input.radius

        border {
            width: 1
            color: control.activeFocus ? Style.color.button.border.selected : Style.color.button.border.primary
        }
    }

    MouseArea {
        anchors.fill: parent

        onWheel: {
            control.value = control.value + (wheel.angleDelta.y > 0 ? 1 : -1)
        }
    }
}
