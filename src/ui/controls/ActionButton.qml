import QtQuick 2.5
import QtGraphicalEffects 1.0

import ui 1.0

Item {
    id: button

    property string text
    property string source
    property int space: 8
    property real iconScale: 1.0

    property bool selected: false
    property bool hasBackground: true
    property bool minimized: false
    property string tooltipText

    property alias image: image

    signal clicked

    Rectangle {
        id: background
        anchors.fill: parent

        radius: Style.button.radius

        property color baseColor: area.containsPress ? Style.color.button.pressed : area.containsMouse ? Style.color.button.hovered : Style.color.button.primary

        color: button.enabled ? selected ? Qt.tint(Style.color.button.selected, Qt.rgba(baseColor.r, baseColor.g, baseColor.b, 0.2)) : baseColor : Style.color.button.primary
        opacity: button.enabled ? 1.0 : 0.5

        border {
            width: 1
            color: selected ? Style.color.button.border.selected : Style.color.button.border.primary
        }

        Behavior on opacity {
            enabled: Config.enableAnimations

            OpacityAnimator {
                duration: 100
            }
        }

        Behavior on color {
            enabled: Config.enableAnimations

            ColorAnimation {
                duration: 100
            }
        }

        visible: false
    }

    DropShadow {
        anchors.fill: background
        source: background
        color: area.containsPress ? "#00000000" : "#10000000"
        samples: 2
        visible: hasBackground

        Behavior on color {
            enabled: Config.enableAnimations

            ColorAnimation {
                duration: 100
            }
        }
    }

    Item {
        anchors.fill: parent

        clip: true

        Item {
            id: image

            anchors {
                top: parent.top
                horizontalCenter: parent.horizontalCenter
                margins: space / 2
            }

            width: height
            height: Math.min(48, button.height - space)

            ColorizedImage {
                anchors {
                    centerIn: parent
                }

                width: Math.round(parent.width * iconScale)
                height: Math.round(parent.height * iconScale)

                source: button.source
                color: button.enabled ? selected ? Style.color.text.selected : area.containsPress ? Style.color.text.secondary : area.containsMouse ? Style.color.text.accent : Style.color.text.primary : Style.color.text.primary
                opacity: button.enabled ? 1.0 : 0.5

                Behavior on color {
                    enabled: Config.enableAnimations

                    ColorAnimation {
                        duration: 100
                    }
                }
            }
        }

        Label {
            id: text

            anchors {
                left: parent.left
                right: parent.right
                top: image.bottom
                leftMargin: space / 2
                rightMargin: space / 2
            }

            text: button.text
            color: selected ? Style.color.text.selected : Style.color.text.accent
            opacity: minimized ? 0.0 : 1.0

            font {
                capitalization: Font.AllUppercase
            }

            wrapMode: Text.WrapAtWordBoundaryOrAnywhere

            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignTop

            Behavior on opacity {
                enabled: Config.enableAnimations

                OpacityAnimator {
                    duration: 100
                }
            }
        }
    }

    TooltipArea {
        id: area
        anchors.fill: parent

        enabled: button.enabled
        text: tooltipText

        cursorShape: Qt.PointingHandCursor

        onClicked: {
            button.clicked()
        }
    }
}
