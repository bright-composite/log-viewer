import QtQuick 2.5
import QtGraphicalEffects 1.0

import ui 1.0
import models 1.0

Item {
    id: listbox

    property var model: [{
        value: 0,
        text: "Значение"
    }]

    property int currentIndex: allowEmpty ? -1 : 0
    property var currentValue: null
    property bool allowEmpty: false
    property string emptyText: "(Нет)"
    property int space: 8

    property string tooltipText

    signal clicked

    implicitWidth: label.contentWidth + 16
    implicitHeight: label.contentHeight + 8

    function findByValue(value) {
        for(var i = 0; i < model.length; ++i) {
            if(model[i].value == value) {
                return i
            }
        }

        return -1
    }

    PropertyConnection {
        targetProperty: "currentIndex"
        function map(value) {
            var index = findByValue(value)
            return index >= 0 ? index : allowEmpty ? -1 : 0
        }

        sourceProperty: "currentValue"
        function unmap(value) {
            return model.length > 0 && value >= 0 ? model[value].value : null
        }
    }

    DropShadow {
        anchors.fill: background
        source: background
        horizontalOffset: 2
        verticalOffset: 2

        color: "#30000000"
        visible: listbox.activeFocus
    }

    Rectangle {
        id: background
        anchors.fill: parent

        radius: Style.button.radius

        color: area.containsPress ? Style.color.button.pressed : area.containsMouse ? Style.color.button.hovered : Style.color.button.primary

        border {
            width: 1
            color: Style.color.button.border.primary
        }

        Behavior on color {
            enabled: Config.enableAnimations

            ColorAnimation {
                duration: 100
            }
        }

        ColorizedImage {
            anchors {
                top: parent.top
                right: parent.right
                bottom: parent.bottom
                margins: 12
            }

            width: height

            source: "qrc:/icons/caret-arrow-small.png"
            color: Style.color.text.primary
            rotation: listbox.activeFocus ? 90 : -90

            Behavior on rotation {
                enabled: Config.enableAnimations

                RotationAnimation {
                    duration: 200
                }
            }
        }
    }

    Label {
        id: label
        anchors.fill: parent

        text: listbox.model.length > 0 && currentIndex >= 0 ? listbox.model[listbox.currentIndex].text : listbox.emptyText
        color: Style.color.text.accent

        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter
    }

    TooltipArea {
        id: area
        anchors.fill: parent

        text: tooltipText

        cursorShape: Qt.PointingHandCursor

        onClicked: {
            if(listbox.activeFocus) {
                root.contentItem.forceActiveFocus()
            } else {
                listbox.forceActiveFocus()
            }
        }

        onWheel: {
            var min = allowEmpty ? -1 : 0
            var max = model.length - 1
            listbox.currentIndex = Math.max(min, Math.min(max, listbox.currentIndex + (wheel.angleDelta.y < 0 ? 1 : -1)))
        }
    }

    Popup {
        id: popup

        property var position: getPosition(listbox)

        x: position.x
        y: position.y + listbox.height - 1
        width: listbox.width
        height: list.height + 2

        resize {
            left: false
            top: false
            right: false
            bottom: false
        }

        shown: listbox.activeFocus

        Column {
            id: list

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                margins: 1
            }

            Repeater {
                model: allowEmpty ? [{text: listbox.emptyText, value: null}].concat(listbox.model) : listbox.model

                Item {
                    id: item

                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    height: 24

                    property int itemIndex: allowEmpty ? index - 1 : index
                    property bool selected: itemIndex == listbox.currentIndex

                    Rectangle {
                        anchors {
                            left: parent.left
                            right: parent.right
                            bottom: item.top
                        }

                        height: 1
                        color: Style.color.border.light
                    }

                    Rectangle {
                        anchors.fill: parent

                        property color baseColor: localArea.containsPress ? Style.color.button.pressed : localArea.containsMouse ? Style.color.button.hovered : Style.color.button.primary

                        color: item.selected ? Qt.tint(Style.color.button.selected, Qt.rgba(baseColor.r, baseColor.g, baseColor.b, 0.2)) : baseColor

                        Behavior on color {
                            enabled: Config.enableAnimations

                            ColorAnimation {
                                duration: 100
                            }
                        }
                    }

                    Label {
                        anchors.fill: parent

                        text: modelData.text
                        color: item.selected ? Style.color.text.selected : Style.color.text.accent

                        horizontalAlignment: Qt.AlignHCenter
                        verticalAlignment: Qt.AlignVCenter
                    }

                    MouseArea {
                        id: localArea
                        anchors.fill: parent

                        cursorShape: Qt.PointingHandCursor
                        hoverEnabled: true

                        onClicked: {
                            listbox.currentIndex = item.itemIndex
                            root.contentItem.forceActiveFocus()
                        }
                    }
                }
            }
        }
    }
}
