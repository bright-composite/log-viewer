import QtQuick 2.0
import QtQuick.Controls 1.4

import ui 1.0

Item {
    id: textArea

    property string content: ""
    property string filterRole: ""
    property int wrapMode: Text.WrapAtWordBoundaryOrAnywhere

    property var highlights: []

    Rectangle {
        anchors {
            fill: scrollable
            margins: -1
        }

        color: Style.color.input
        radius: Style.input.radius

        border {
            width: 1
            color: Style.color.border.accent
        }
    }

    Scrollable {
        id: scrollable

        Item {
            width: edit.contentWidth
            height: edit.contentHeight + 20

            SelectableText {
                id: edit

                width: textArea.width

                content: textArea.content
                filterRole: textArea.filterRole
                highlights: textArea.highlights

                textMargin: Style.font.size.primary
                wrapMode: textArea.wrapMode
                elide: Qt.ElideNone

                text: elidedText

                function getHighlightedText(text) {
                    var parts = []
                    var last = 0
                    var style = 'style="background-color: ' + Style.color.text.highlight + ';"'

                    function raw(start, end) {
                        parts.push(replaceAll(text.slice(start, end), "\n", '</p><p>'))
                        last = end
                    }

                    function highlight(start, end) {
                        parts.push('<span ' + style + '>' + replaceAll(text.slice(start, end), "\n", '</span></p><p><span ' + style + '>') + '</span>')
                        last = end
                    }

                    function output() {
                        return "<p>" + parts.join("") + "</p>"
                    }

                    for(var i = 0; i < highlights.length; ++i) {
                        if(highlights[i].start >= text.length) {
                            raw(last, text.length - 1)
                            parts.push('<span ' + style + '>…</span>')
                            return output()
                        }

                        if(highlights[i].start > last) {
                            raw(last, highlights[i].start)
                        }

                        if(highlights[i].end >= text.length) {
                            highlight(highlights[i].start, text.length - 1)
                            return output()
                        }

                        highlight(highlights[i].start, highlights[i].end)
                    }

                    raw(last)
                    return output()
                }
            }
        }
    }
}
