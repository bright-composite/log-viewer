import QtQuick 2.0

import ui 1.0

Text {
    color: Style.color.text.primary

    font {
        family: Style.font.primary.name
        pointSize: Style.font.size.primary
    }
}
