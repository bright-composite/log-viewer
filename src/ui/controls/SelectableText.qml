import QtQuick 2.5
import QtQuick.Controls 1.4

import ui 1.0

TextEdit {
    id: edit

    property var highlights: []

    property string content: ""
    property string filterRole: ""
    readonly property string elidedText: getHighlightedText(replaceAll(metrics.elidedText, "  ", " "))
    property int elide: Qt.ElideRight

    text: replaceAll(elidedText, "\n", " ")

    function replaceAll(str, find, replace) {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    function getHighlightedText(text) {
        var parts = []
        var last = 0
        var style = 'style="background-color: ' + Style.color.text.highlight + ';"'

        function raw(start, end) {
            parts.push(text.slice(start, end))
            last = end
        }

        function highlight(start, end) {
            parts.push('<span ' + style + '>' + text.slice(start, end) + '</span>')
            last = end
        }

        function output() {
            return parts.join("")
        }

        for(var i = 0; i < highlights.length; ++i) {
            if(highlights[i].start >= text.length) {
                raw(last, text.length - 1)
                parts.push('<span ' + style + '>…</span>')
                return output()
            }

            if(highlights[i].start > last) {
                raw(last, highlights[i].start)
            }

            if(highlights[i].end >= text.length) {
                highlight(highlights[i].start, text.length)
                return output()
            }

            highlight(highlights[i].start, highlights[i].end)
        }

        raw(last)
        return output()
    }

    color: Style.color.text.primary
    selectionColor: Style.color.selected
    selectedTextColor: Style.color.text.selected

    font {
        pointSize: Style.font.size.primary
        family: Style.font.mono.name
    }

    selectByMouse: true
    readOnly: true
    textFormat: Text.RichText

    textMargin: 0
    verticalAlignment: Qt.AlignTop
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere

    signal selected

    onActiveFocusChanged: {
        if(activeFocus) {
            root.context.edit = edit
            selected()
        } else {
            if(root.context.edit == edit) {
                root.context.edit = null
            }

            deselect()
        }
    }

    TextMetrics {
        id: metrics

        text: edit.content
        elideWidth: edit.width - 4
        elide: edit.elide

        font {
            family: edit.font.family
            pointSize: edit.font.pointSize
        }
    }

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.RightButton

        onClicked: {
            edit.forceActiveFocus()
            root.context.popup()
        }
    }
}
