import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import ui 1.0

Calendar {
    id: calendar
    z: 200

    style: CalendarStyle {
        gridColor: Style.color.border.light

        navigationBar: Rectangle {
            implicitHeight: 36
            color: Style.color.secondary
            z: 200

            Label {
                anchors.fill: parent
                text: styleData.title
                font.pointSize: 13
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
            }

            TextButton {
                anchors {
                    top: parent.top
                    bottom: parent.bottom
                    left: parent.left
                }

                width: height
                text: "<"
                hasBackground: false
                visible: calendar.visibleYear > calendar.minimumDate.getFullYear() || (calendar.visibleYear == calendar.minimumDate.getFullYear() && calendar.visibleMonth > calendar.minimumDate.getMonth())

                onClicked: {
                    calendar.showPreviousMonth()
                }
            }

            TextButton {
                anchors {
                    top: parent.top
                    bottom: parent.bottom
                    right: parent.right
                }

                width: height
                text: ">"
                hasBackground: false
                visible: calendar.visibleYear < calendar.maximumDate.getFullYear() || (calendar.visibleYear == calendar.maximumDate.getFullYear() && calendar.visibleMonth < calendar.maximumDate.getMonth())

                onClicked: {
                    calendar.showNextMonth()
                }
            }
        }

        background: Rectangle {
            anchors.fill: parent
            color: Style.color.secondary
        }

        dayDelegate: Rectangle {
            color: styleData.selected ? Style.color.selected : Style.color.input

            Label {
                text: styleData.date.getDate()
                anchors.centerIn: parent
                color: styleData.selected ? Style.color.text.selected : styleData.valid ? Style.color.text.primary : Style.color.text.light
            }
        }

        dayOfWeekDelegate: Rectangle {
            implicitWidth: calendar.width / 7
            implicitHeight: 24

            color: Style.color.accent

            Label {
                anchors.fill: parent
                text: Qt.locale().standaloneDayName(styleData.dayOfWeek, Locale.ShortFormat)
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter

                font {
                    pointSize: Style.font.size.small
                }
            }
        }
    }
}
