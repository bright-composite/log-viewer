import QtQuick 2.5
import QtGraphicalEffects 1.0

import ui 1.0

Item {
    id: button

    property string text
    property int space: 8

    property bool selected: false
    property string tooltipText
    property bool hasBackground: true

    signal clicked

    implicitWidth: label.contentWidth + 16
    implicitHeight: label.contentHeight + 8

    Rectangle {
        id: background
        anchors.fill: parent

        radius: Style.button.radius

        property color baseColor: area.containsPress ? Style.color.button.pressed : area.containsMouse ? Style.color.button.hovered : Style.color.button.primary

        color: selected ? Qt.tint(Style.color.button.selected, Qt.rgba(baseColor.r, baseColor.g, baseColor.b, 0.2)) : baseColor

        border {
            width: 1
            color: selected ? Style.color.button.border.selected : Style.color.button.border.primary
        }

        Behavior on opacity {
            enabled: Config.enableAnimations

            OpacityAnimator {
                duration: 100
            }
        }

        Behavior on color {
            enabled: Config.enableAnimations

            ColorAnimation {
                duration: 100
            }
        }

        visible: false
    }

    DropShadow {
        anchors.fill: background
        source: background
        color: area.containsPress ? "#00000000" : "#10000000"
        samples: 2

        Behavior on color {
            enabled: Config.enableAnimations

            ColorAnimation {
                duration: 100
            }
        }

        visible: hasBackground
    }

    Label {
        id: label
        anchors.fill: parent

        text: button.text
        color: selected ? Style.color.text.selected : Style.color.text.accent

        font {
            capitalization: Font.AllUppercase
        }

        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter
    }

    TooltipArea {
        id: area
        anchors.fill: parent

        text: tooltipText

        cursorShape: Qt.PointingHandCursor

        onClicked: {
            button.clicked()
        }
    }
}
