import QtQuick 2.0
import Qt.labs.controls 1.0

import ui 1.0

CheckBox {
    id: control

    property bool small: false

    height: small ? 14 : 24

    topPadding: 0
    bottomPadding: 0
    leftPadding: 0
    rightPadding: 0

    indicator: Rectangle {
        x: text ? (control.mirrored ? control.width - width - control.rightPadding : control.leftPadding) : control.leftPadding + (control.availableWidth - width) / 2
        y: control.topPadding + (control.availableHeight - height) / 2

        implicitWidth: small ? 14 : 20
        implicitHeight: small ? 14 : 20

        color: Style.color.input

        border {
            width: 1
            color: Style.color.button.border.primary
        }

        radius: Style.input.radius

        ColorizedImage {
            id: image

            anchors {
                fill: parent
                margins: 1
            }

            source: "qrc:/qt-project.org/imports/Qt/labs/controls/images/check.png"
            color: Style.color.text.primary

            visible: control.checked
        }
    }

    label: Label {
        x: control.mirrored ? control.leftPadding : (indicator.x + indicator.width + control.spacing)
        y: control.topPadding

        width: control.availableWidth - indicator.width - control.spacing
        height: control.availableHeight

        text: control.text

        font {
            pointSize: small ? Style.font.size.small : 12
        }

        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter
    }
}
