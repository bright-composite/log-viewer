import QtQuick 2.0
import QtQuick.Layouts 1.1

import ui 1.0
import models 1.0

Item {
    id: input

    property string text: ""
    property date dateTime: DateTime.dateTimeFromString("")

    property date minimumDate
    property date maximumDate

    function setDateTime(value) {
        dateTime = value
        dateInput.date = DateTime.date(dateTime)
        timeInput.time = DateTime.time(dateTime)
    }

    PropertyConnection {
        target: input

        targetProperty: "dateTime"

        function map(value) {
            var dt = DateTime.dateTimeFromString(value)

            if(value) {
                if(dt > maximumDate) {
                    input.text = DateTime.dateToString(dt = maximumDate)
                }

                if(dt < minimumDate) {
                    input.text = DateTime.dateToString(dt = minimumDate)
                }
            }

            return dt
        }

        sourceProperty: "text"
        function unmap(value) { return DateTime.dateTimeToString(value) }
    }

    RowLayout {
        anchors.fill: parent

        spacing: 8

        CalendarInput {
            id: dateInput

            Layout.fillWidth: true

            minimumDate: input.minimumDate
            maximumDate: input.maximumDate

            color: Style.color.text.primary
            placeholder: "ГГГГ/ММ/ДД"

            onDateChanged: {
                input.dateTime = DateTime.dateTime(date, DateTime.time(input.dateTime))
            }
        }

        TimeInput {
            id: timeInput

            Layout.fillWidth: true

            color: Style.color.text.primary
            placeholder: "чч:мм:сс"

            onTimeChanged: {
                input.dateTime = DateTime.dateTime(DateTime.date(input.dateTime), time)
            }
        }
    }

    Component.onCompleted: {
        dateInput.date = DateTime.date(dateTime)
        timeInput.time = DateTime.time(dateTime)
    }
}
