import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import ui 1.0
import models 1.0

Item {
    id: edit

    property alias area: area

    property Item prependDelegate
    property Item appendDelegate

    property string text: ""
    property string placeholder: ""

    property color color: Style.color.text.primary
    property color placeholderColor: Style.color.text.light

    property QtObject validator
    property int horizontalAlignment: Qt.AlignLeft
    property int verticalAlignment: Qt.AlignVCenter
    property int inputMethodHints: Qt.ImhNoPredictiveText

    property int echoMode: TextInput.Normal
    property int passwordMaskDelay: 0

    property bool acceptableInput: field.acceptableInput
    property alias field: field

    property int textMargin: 5

    signal edited
    signal accepted

    readonly property bool focused: field.activeFocus

    height: 32

    onPrependDelegateChanged: {
        if(prependDelegate) {
            prependDelegate.parent = prepend
        }
    }

    onAppendDelegateChanged: {
        if(appendDelegate) {
            appendDelegate.parent = append
        }
    }

    Rectangle {
        id: area
        anchors.fill: parent
        color: Style.color.input

        radius: Style.input.radius
    }

    TextField {
        id: field

        anchors {
            fill: parent
            leftMargin: textMargin + prepend.width
            rightMargin: textMargin + append.width
        }

        textColor: edit.color

        echoMode: edit.echoMode

        validator: edit.validator
        horizontalAlignment: edit.horizontalAlignment
        verticalAlignment: edit.verticalAlignment
        inputMethodHints: edit.inputMethodHints
        selectByMouse: true

        font {
            family: Style.font.primary.name
            pointSize: Style.font.size.primary
        }

        placeholderText: placeholder

        style: TextFieldStyle {
            textColor: Style.color.text.primary
            selectionColor: Style.color.selected
            selectedTextColor: Style.color.text.selected

            placeholderTextColor: edit.placeholderColor

            background: Item {}
        }

        onEditingFinished: {
            edit.edited()
        }

        onAccepted: {
            edit.accepted()
        }

        PropertyConnection {
            target: edit
            source: field
            property: "text"
        }
    }

    Item {
        id: prepend

        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
        }

        width: children.length > 0 ? children[0].width : 0
        visible: children.length > 0
        clip: true
    }

    Item {
        id: append

        anchors {
            top: parent.top
            bottom: parent.bottom
            right: parent.right
        }

        width: children.length > 0 ? children[0].width : 0
        visible: children.length > 0
        clip: true
    }

    Rectangle {
        anchors.fill: parent
        color: "transparent"

        border {
            width: 1
            color: field.activeFocus ? Style.color.button.border.selected : Style.color.button.border.primary
        }

        radius: Style.input.radius
    }
}
