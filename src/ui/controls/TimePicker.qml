import QtQuick 2.0

import ui 1.0
import models 1.0

Item {
    id: picker
    property var selectedTime: DateTime.time()

    Rectangle {
        anchors.fill: parent
        color: Style.color.secondary

        border {
            width: 1
            color: Style.color.border.primary
        }
    }

    Column {
        anchors.centerIn: parent

        width: childrenRect.width

        spacing: 8

        Spinbox {
            from: 0
            to: 23

            PropertyConnection {
                target: picker
                targetProperty: "selectedTime"
                function map(value) {
                    return DateTime.time(value, picker.selectedTime.minute, picker.selectedTime.second)
                }

                source: parent
                sourceProperty: "value"
                function unmap(value) {
                    return value.hour
                }
            }
        }

        Spinbox {
            from: 0
            to: 59

            PropertyConnection {
                target: picker
                targetProperty: "selectedTime"
                function map(value) {
                    return DateTime.time(picker.selectedTime.hour, value, picker.selectedTime.second)
                }

                source: parent
                sourceProperty: "value"
                function unmap(value) {
                    return value.minute
                }
            }
        }

        Spinbox {
            from: 0
            to: 59

            PropertyConnection {
                target: picker
                targetProperty: "selectedTime"
                function map(value) {
                    return DateTime.time(picker.selectedTime.hour, picker.selectedTime.minute, value)
                }

                source: parent
                sourceProperty: "value"
                function unmap(value) {
                    return value.second
                }
            }
        }
    }
}
