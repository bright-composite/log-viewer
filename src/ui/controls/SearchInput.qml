import QtQuick 2.0

import ui 1.0
import models 1.0

Item {
    id: searchInput

    property string text: ""
    property bool regexp: false
    property bool caseSensitive: true

    signal search(string text, bool regexp, bool caseSensitive)

    Input {
        id: input

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }

        height: 36
        z: 1

        placeholder: "Найти..."

        field {
            font {
                pointSize: 11
            }
        }

        PropertyConnection {
            target: searchInput
            source: input
            property: "text"
        }

        appendDelegate: Row {
            height: parent.height
            spacing: 4

            ImageButton {
                anchors {
                    verticalCenter: parent.verticalCenter
                }

                width: input.height - 16
                height: input.height - 16

                source: "qrc:/icons/close.png"
                iconScale: 0.5
                visible: input.text != ""
                hasBackground: false

                tooltipText: "Очистить"

                onClicked: {
                    input.text = ""
                    input.parent.forceActiveFocus()
                }
            }

            ImageButton {
                anchors {
                    top: parent.top
                    bottom: parent.bottom
                }

                width: height

                enabled: input.text != ""
                source: "qrc:/icons/search.png"
                iconScale: 0.6

                tooltipText: "Поиск (Ctrl+F)"

                onClicked: {
                    searchInput.search(searchInput.text, searchInput.regexp, searchInput.caseSensitive)
                }
            }
        }
    }

    Item {
        anchors {
            left: input.left
            right: input.right
            top: input.bottom
        }

        height: searchInput.height >= 72 || input.focused ? 36 : 0

        visible: height > 0

        Behavior on height {
            enabled: Config.enableAnimations

            NumberAnimation {
                duration: 100
                easing {
                    type: Easing.OutQuad
                }
            }
        }

        Rectangle {
            anchors.fill: parent
            visible: searchInput.height < 72
            color: Style.color.primary

            border {
                width: 1
                color: Style.color.border.primary
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
            }
        }

        Column {
            anchors {
                bottom: parent.bottom
                left: parent.left
                right: parent.right
                leftMargin: searchInput.height < 72 ? 4 : 0
                rightMargin: searchInput.height < 72 ? 4 : 0
                bottomMargin: 4
            }

            spacing: 1

            Behavior on anchors.leftMargin {
                enabled: Config.enableAnimations

                NumberAnimation {
                    duration: 100
                }
            }

            Behavior on anchors.rightMargin {
                enabled: Config.enableAnimations

                NumberAnimation {
                    duration: 100
                }
            }

            Checkbox {
                id: regexp
                small: true
                text: "Регулярное выражение"
                checked: false

                PropertyConnection {
                    target: searchInput
                    targetProperty: "regexp"
                    source: regexp
                    sourceProperty: "checked"
                }
            }

            Checkbox {
                id: caseSensitive
                small: true
                text: "Учитывать регистр"
                checked: true

                PropertyConnection {
                    target: searchInput
                    targetProperty: "caseSensitive"
                    source: caseSensitive
                    sourceProperty: "checked"
                }
            }
        }
    }
}
