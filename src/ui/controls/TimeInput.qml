import QtQuick 2.0

import ui 1.0
import models 1.0

Input {
    id: input

    property var time: DateTime.time()

    validator: RegExpValidator {
        regExp: /\d{2}:\d{2}:\d{2}/
    }

    field.font {
        family: Style.font.mono.name
        pointSize: 11
    }

    PropertyConnection {
        targetProperty: "time"
        function map(value) { return DateTime.timeFromString(value) }

        sourceProperty: "text"
        function unmap(value) { return DateTime.timeToString(value) }
    }

    appendDelegate: ImageButton {
        anchors {
            verticalCenter: parent.verticalCenter
            right: parent.right
            rightMargin: 4
        }

        width: input.height - 16
        height: input.height - 16

        source: "qrc:/icons/close.png"
        iconScale: 0.5
        visible: input.text != ""
        hasBackground: false

        tooltipText: "Очистить"

        onClicked: {
            input.text = ""
        }
    }

    TimePopup {
        id: popup

        property var position: getPosition(input)
        focus: true

        x: Math.min(position.x + (input.width - width) / 2, parent.width - width - 20)
        y: position.y + input.height
        width: Math.max(input.width, 160)
        height: width

        PropertyConnection {
            target: input
            targetProperty: "time"
            source: popup
            sourceProperty: "selectedTime"
        }

        shown: input.visible && (input.focused || popup.focused)
    }
}
