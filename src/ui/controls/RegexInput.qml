import QtQuick 2.0

import "."

Input {
    id: input
    property bool regex: false

    appendDelegate: TextButton {
        anchors {
            top: parent.top
            bottom: parent.bottom
        }

        width: height

        text: ".*"
        selected: input.regex
        tooltipText: "Регулярное выражение"

        onClicked: {
            input.regex = !input.regex
        }
    }
}
