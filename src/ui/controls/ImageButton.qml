import QtQuick 2.5
import QtGraphicalEffects 1.0

import ui 1.0

Item {
    id: button

    property string source
    property int space: 8
    property real iconScale: 1.0

    property bool selected: false
    property bool hasBackground: true
    property string tooltipText

    signal clicked

    Rectangle {
        id: background
        anchors.fill: parent

        radius: Style.button.radius

        property color baseColor: area.containsPress ? Style.color.button.pressed : area.containsMouse ? Style.color.button.hovered : Style.color.button.primary

        color: button.enabled ? selected ? Qt.tint(Style.color.button.selected, Qt.rgba(baseColor.r, baseColor.g, baseColor.b, 0.2)) : baseColor : Style.color.button.primary
        opacity: button.enabled ? 1.0 : 0.5

        border {
            width: 1
            color: selected ? Style.color.button.border.selected : Style.color.button.border.primary
        }

        Behavior on opacity {
            enabled: Config.enableAnimations

            OpacityAnimator {
                duration: 100
            }
        }

        Behavior on color {
            enabled: Config.enableAnimations

            ColorAnimation {
                duration: 100
            }
        }

        visible: false
    }

    DropShadow {
        anchors.fill: background
        source: background
        color: area.containsPress ? "#00000000" : "#10000000"
        samples: 2
        visible: hasBackground

        Behavior on color {
            enabled: Config.enableAnimations

            ColorAnimation {
                duration: 100
            }
        }
    }

    ColorizedImage {
        id: image
        anchors.centerIn: background

        width: Math.floor(parent.width * iconScale)
        height: Math.floor(parent.height * iconScale)

        source: button.source
        color: button.enabled ? selected ? Style.color.text.selected : area.containsPress ? Style.color.text.secondary : area.containsMouse ? Style.color.text.accent : Style.color.text.primary : Style.color.text.primary
        opacity: button.enabled ? 1.0 : 0.5

        Behavior on color {
            enabled: Config.enableAnimations

            ColorAnimation {
                duration: 100
            }
        }
    }

    TooltipArea {
        id: area
        anchors.fill: parent

        enabled: button.enabled
        text: tooltipText

        cursorShape: Qt.PointingHandCursor

        onClicked: {
            button.clicked()
        }
    }
}
