pragma Singleton

import QtQuick 2.0

QtObject {
    function forAdapter(table, model, root, callback) {
        var component

        function create() {
            callback(component.createObject(table, {table: table, model: model, root: root}))
        }

        function tryComponent(name, error) {
            component = Qt.createComponent("qrc:/ui/adapters/" + name + "UI.qml")

            if(component.status == Component.Ready) {
                return create()
            }

            if (component.status == Component.Error) {
                error()
            }

            component.statusChanged.connect(function() {
                if (component.status == Component.Ready) {
                    create()
                } else if (component.status == Component.Error) {
                    error()
                }
            })
        }

        tryComponent(model.adapter.key, function() {
            tryComponent("GenericAdapter", function() {
                console.error("Nothing to do here...")
            })
        })
    }
}
