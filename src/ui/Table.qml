import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import ui 1.0
import test.logs 1.0

FluidTableView {
    id: view

    property ModelAdapter adapter
    property var searchOccurrencies: ({})

    property int scrollBarWidth: 10

    signal dropped(string url)

    function findOccurrencies(occurrencies, row, role) {
        if(!occurrencies.hasOwnProperty(row)) {
            return []
        }

        var list = occurrencies[row]
        var roleId = adapter.role(role)
        var out = []

        for(var i = 0; i < list.length; ++i) {
            if(list[i].role == roleId) {
                out.push(list[i])
            }
        }

        return out
    }

    style: TableStyle {}

    rowDelegate: Rectangle {
        color: styleData.selected ? Style.color.accent : (styleData.alternate ? Style.color.alternate : Style.color.secondary)
        height: 39
    }

    itemDelegate: SelectableText {
        anchors.fill: parent
        anchors.margins: 10

        content: styleData.value || ""
        filterRole: table.getColumn(styleData.column).role

        highlights: table.findOccurrencies(table.searchOccurrencies, table.model.rowId(styleData.row), filterRole)

        font {
            family: Style.font.primary.name
        }

        onSelected: {
            table.selection.clear()
            table.selection.select(styleData.row, styleData.row)
        }

        horizontalAlignment: styleData.textAlignment
        verticalAlignment: Qt.AlignVCenter
        color: styleData.selected ? Style.color.text.accent : Style.color.text.primary
        elide: styleData.elideMode
        wrapMode: styleData.selected ? Text.WordWrap : Text.NoWrap
    }

    headerDelegate: Rectangle {
        id: headerCell

        height: 28
        color: Style.color.alternate

        Rectangle {
            anchors {
                bottom: parent.top
                left: parent.left
                right: parent.right
            }

            height: 2

            color: Style.color.accent
        }

        CellBorders {
            table: view
        }

        Rectangle {
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }

            height: 1

            color: "#10ffffff"
        }

        Label {
            text: styleData.value
            anchors.fill: parent
            color: Style.color.text.accent
            font.pointSize: 10
            font.weight: Font.DemiBold
            clip: true
            elide: Qt.ElideRight
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
        }

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
        }

        SideShadow {
            size: 4
        }
    }

    Rectangle {
        anchors.fill: parent
        color: dropArea.containsDrag ? "#20000000" : "transparent"

        DropArea {
            id: dropArea
            anchors.fill: parent
            keys: ["text/plain"]

            onDropped: {
                if(drop.urls.length > 0) {
                    view.dropped(drop.urls[0])
                }
            }
        }
    }

    function hasResizer(column) {
        return view.getColumn(column) && view.getColumn(column).resizable
    }
}
