import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0

import "."

Resizable {
    id: details

    property var model
    property int selectedRow: -1
    property bool wrapText: true

    property alias header: header
    property alias message: message

    signal close

    implicitWidth: minimumWidth
    implicitHeight: minimumHeight

    minimumWidth: 300
    minimumHeight: 300

    DropShadow {
        anchors.fill: background
        source: background
        horizontalOffset: 2
        verticalOffset: 2

        visible: movable

        color: "#30000000"
    }

    Rectangle {
        id: background

        anchors {
            fill: parent
        }

        color: Style.color.accent

        MouseArea {
            anchors.fill: parent
            focus: true
            propagateComposedEvents: true

            onClicked: {
                forceActiveFocus()
                mouse.accepted = false
            }
        }
    }

    Rectangle {
        id: header

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }

        height: 28

        color: Style.color.alternate

        SideShadow {
            size: 4
        }

        z: 101

        Label {
            anchors.fill: parent

            text: "Подробности"
            color: Style.color.text.accent

            font {
                pointSize: 12
            }

            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
        }

        Item {
            anchors {
                top: parent.top
                right: parent.right
                margins: 2
            }

            width: 24
            height: 24

            z: 102

            ImageButton {
                width: 24
                height: 24

                source: "qrc:/icons/close.png"
                iconScale: 0.6

                z: 103

                tooltipText: "Закрыть"

                onClicked: {
                    details.close()
                }
            }
        }
    }

    Item {
        anchors {
            top: header.bottom
            left: parent.left
            bottom: parent.bottom
        }

        width: Math.max(300, parent.width)
        clip: true

        ScrollView {
            anchors {
                fill: parent
            }

            style: ScrollStyle {}

            Flickable {
                id: flickable

                anchors {
                    fill: parent
                    topMargin: 24
                }

                contentHeight: Math.max(column.height + 360, height)

                Column {
                    id: column

                    anchors {
                        left: parent.left
                        right: parent.right
                        leftMargin: 32
                        rightMargin: 32
                    }

                    spacing: 32

                    Item {
                        anchors {
                            left: parent.left
                            right: parent.right
                        }

                        height: description.childrenRect.height

                        GridLayout {
                            id: description

                            anchors {
                                left: parent.left
                                right: parent.right
                                rightMargin: column.width < 340 ? 0 : 24
                            }

                            rowSpacing: 32
                            columnSpacing: 32

                            columns: column.width < 340 ? 1 : 2

                            ColorizedImage {
                                Layout.preferredWidth: model ? column.width < 340 ? details.width * 0.33 : 100 : 0
                                Layout.preferredHeight: model ? column.width < 340 ? details.width * 0.33 : 100 : 0
                                Layout.alignment: Qt.AlignCenter

                                source: {
                                    if(model) {
                                        switch(model.severity) {
                                            case "error":
                                                return "qrc:/icons/error.png"
                                            case "warning":
                                                return "qrc:/icons/warning.png"
                                            default:
                                                return "qrc:/icons/info.png"
                                        }
                                    }

                                    return ""
                                }

                                color: {
                                    if(model) {
                                        switch(model.severity) {
                                            case "error":
                                                return Style.color.error
                                            case "warning":
                                                return Style.color.warning
                                            case "notice":
                                                return Style.color.notice
                                            default:
                                                return Style.color.primary
                                        }
                                    }

                                    return Style.color.primary
                                }

                                Behavior on width {
                                    enabled: Config.enableAnimations

                                    NumberAnimation {
                                        duration: 100
                                    }
                                }

                                Behavior on height {
                                    enabled: Config.enableAnimations

                                    NumberAnimation {
                                        duration: 100
                                    }
                                }
                            }

                            Item {
                                Layout.preferredWidth: 256
                                Layout.preferredHeight: info.childrenRect.height
                                Layout.fillWidth: true
                                Layout.alignment: Qt.AlignCenter

                                GridLayout {
                                    id: info
                                    anchors {
                                        left: parent.left
                                        right: parent.right
                                    }

                                    columns: 2
                                    rowSpacing: 8
                                    columnSpacing: 16

                                    Label {
                                        Layout.alignment: Qt.AlignTop

                                        text: "Строка:"

                                        color: Style.color.text.accent
                                        elide: Qt.ElideRight
                                    }

                                    SelectableText {
                                        Layout.fillWidth: true
                                        Layout.preferredHeight: 16 * lineCount
                                        Layout.alignment: Qt.AlignTop

                                        content: model ? model.line : "---"
                                        color: Style.color.text.primary
                                    }

                                    Label {
                                        Layout.alignment: Qt.AlignTop

                                        text: "Время:"

                                        color: Style.color.text.accent
                                        elide: Qt.ElideRight
                                    }

                                    SelectableText {
                                        Layout.fillWidth: true
                                        Layout.preferredHeight: 16 * lineCount
                                        Layout.alignment: Qt.AlignTop

                                        content: model ? (new Date(model.time)).toLocaleString(Qt.locale(), "yyyy/MM/dd HH:mm:ss.zzz t") : "---"
                                        color: Style.color.text.primary
                                    }

                                    Label {
                                        Layout.alignment: Qt.AlignTop
                                        text: "Модуль:"

                                        color: Style.color.text.accent
                                        elide: Qt.ElideRight
                                    }

                                    SelectableText {
                                        Layout.fillWidth: true
                                        Layout.preferredHeight: 16 * lineCount
                                        Layout.alignment: Qt.AlignTop

                                        content: model ? model.unit : "---"
                                        color: Style.color.text.primary
                                    }

                                    Label {
                                        Layout.alignment: Qt.AlignTop
                                        text: "Уровень:"

                                        color: Style.color.text.accent
                                        elide: Qt.ElideRight
                                    }

                                    SelectableText {
                                        Layout.fillWidth: true
                                        Layout.preferredHeight: 16 * lineCount
                                        Layout.alignment: Qt.AlignTop

                                        content: model ? model.level : "---"
                                        color: Style.color.text.primary
                                    }
                                }
                            }
                        }
                    }
                }

                SelectableTextArea {
                    id: message

                    anchors {
                        top: column.bottom
                        left: parent.left
                        right: parent.right
                        margins: 32
                    }

                    height: flickable.contentHeight - column.height - 72

                    content: model ? model.message : "---"
                    filterRole: "message"
                    wrapMode: details.wrapText ? Text.WrapAtWordBoundaryOrAnywhere : Text.NoWrap
                }

                Column {
                    anchors {
                        top: parent.top
                        right: parent.right
                        rightMargin: 19
                    }

                    width: 32

                    spacing: 8

                    ImageButton {
                        width: 32
                        height: 32

                        source: "qrc:/icons/line-wrap.png"
                        iconScale: 0.6
                        selected: details.wrapText

                        tooltipText: details.wrapText ? "Развернуть строки" : "Свернуть строки"

                        onClicked: {
                            details.wrapText = !details.wrapText
                        }
                    }
                }
            }
        }
    }

    Rectangle {
        id: frame

        anchors {
            fill: parent
            leftMargin: resize.left ? 0 : -1
            rightMargin: resize.right ? 0 : -1
            topMargin: resize.top ? 0 : -1
            bottomMargin: resize.bottom ? 0 : -1
        }

        color: "transparent"
        z: 200

        border {
            width: 1
            color: Style.color.border.accent
        }
    }

}
