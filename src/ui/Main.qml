import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

import test.logs 1.0

import "."

ApplicationWindow {
    id: root

    visible: true
    width: 1024
    height: 768
    title: "Log Viewer"

    color: Style.color.accent

    minimumWidth: 640
    minimumHeight: 480

    property alias header: header
    property alias context: context
    property alias tooltip: tooltip
    property alias table: table
    property alias details: details
    property alias statusBar: statusBar

    property bool loaded: false

    signal openFile

    signal filter

    signal setStartTime(date time)
    signal setEndTime(date time)
    signal setFilter(var filter)

    signal applyFilter
    signal resetFilter
    signal clearFilterOptions

    signal search(string text, bool regexp, bool caseSensitive)
    signal resetSearch

    signal settings

    Shortcut {
        sequence: StandardKey.Find

        enabled: !context.canSearch && root.loaded && header.searchInput.text

        onActivated: {
            var input = header.searchInput
            root.search(input.text, input.regexp, input.caseSensitive)
        }
    }

    Shortcut {
        sequence: "Ctrl+Shift+F"

        onActivated: {
            root.resetSearch()
        }
    }

    Shortcut {
        sequence: "Ctrl+G"

        enabled: !root.context.canFilter && root.loaded

        onActivated: {
            root.filter()
        }
    }

    Shortcut {
        sequence: "Ctrl+Shift+G"

        enabled: !root.context.canResetFilter && root.loaded

        onActivated: {
            root.clearFilterOptions()
        }
    }

    Shortcut {
        sequence: "Ctrl+O"

        onActivated: {
            root.settings()
        }
    }

    Menu {
        id: context

        property SelectableText edit: null
        property string filterRole: ""

        property bool canSelect: context.edit
        property bool canFilter: context.edit && context.edit.filterRole && root.loaded
        property bool canResetFilter: context.filterRole && context.edit && context.edit.filterRole == filterRole
        property bool canCopy: context.edit
        property bool canSearch: context.edit && root.loaded

        MenuItem {
            text: "Выделить все"
            shortcut: StandardKey.SelectAll
            enabled: context.canSelect
            visible: context.canSelect

            onTriggered: {
                context.edit.selectAll()
            }
        }

        MenuItem {
            text: "Копировать"
            shortcut: StandardKey.Copy
            enabled: context.canCopy
            visible: context.canCopy

            onTriggered: {
                if(!context.edit.selectedText) {
                    context.edit.selectAll()
                }

                context.edit.copy()
            }
        }

        MenuItem {
            text: "Найти"
            shortcut: StandardKey.Find
            enabled: context.canSearch
            visible: context.canSearch

            onTriggered: {
                header.searchInput.text = context.edit.selectedText || context.edit.content
                header.searchInput.regexp = false
                header.searchInput.caseSensitive = true

                root.search(context.edit.selectedText || context.edit.content, false, true)
            }
        }

        MenuItem {
            text: "Применить как фильтр"
            shortcut: "Ctrl+G"
            enabled: context.canFilter && root.loaded
            visible: context.canFilter && root.loaded

            onTriggered: {
                var filter = {}
                context.filterRole = context.edit.filterRole
                filter[context.edit.filterRole] = { expr: context.edit.selectedText || context.edit.content, syntax: EventListModel.FixedString }
                root.setFilter(filter)
                root.applyFilter()
            }
        }

        MenuItem {
            text: "Сбросить фильтр"
            shortcut: "Ctrl+Shift+G"
            enabled: context.canResetFilter
            visible: context.canResetFilter

            onTriggered: {
                root.resetFilter()
            }
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            parent.forceActiveFocus()
        }
    }

    DockArea {
        id: dock

        anchors {
            top: header.bottom
            bottom: statusBar.top
            left: parent.left
            right: parent.right
        }

        currentDockable: details.dockable.dragging ? details.dockable : null

        Rectangle {
            anchors {
                bottom: parent.bottom
                left: parent.left
                right: parent.right
            }

            height: 1

            color: Qt.tint(Style.color.primary, "#20000000")
        }

        Table {
            id: table

            anchors {
                top: dock.sideTop.bottom
                left: dock.sideLeft.right
                right: dock.sideRight.left
                bottom: dock.sideBottom.top
                bottomMargin: 1
            }

            frameVisible: false
            sortIndicatorVisible: true
        }

        Details {
            id: details

            property bool opened: false

            maximumWidth: dockable.undocked ? Math.max(600, dock.width * 0.8) : dock.width * 0.8
            maximumHeight: dockable.undocked ? Math.max(600, dock.height * 0.8) : dock.height * 0.8

            visible: opened

            property Dockable dockable: Dockable {
                target: details
                dockArea: dock

                dragArea {
                    height: details.header.height
                }
            }

            movable: dockable.undocked

            resize {
                left: dockable.position == Qt.AlignCenter || dockable.position == Qt.AlignRight
                top: dockable.position == Qt.AlignCenter || dockable.position == Qt.AlignBottom
                right: dockable.position == Qt.AlignCenter || dockable.position == Qt.AlignLeft
                bottom: dockable.position == Qt.AlignCenter || dockable.position == Qt.AlignTop

                leftTop: dockable.position == Qt.AlignCenter
                rightTop: dockable.position == Qt.AlignCenter
                leftBottom: dockable.position == Qt.AlignCenter
                rightBottom: dockable.position == Qt.AlignCenter
            }

            message {
                highlights: table.findOccurrencies(table.searchOccurrencies, selectedRow, message.filterRole)
            }

            onOpenedChanged: {
                if(details.opened) {
                    if(dockable.position == Qt.AlignCenter) {
                        dockable.position = dockable.lastPosition
                    }
                }
            }

            onClose: {
                opened = false
                table.selection.clear()
            }
        }
    }

    Header {
        id: header

        filteringEnabled: root.loaded
        searchEnabled: root.loaded
    }

    StatusBar {
        id: statusBar
    }

    Tooltip {
        id: tooltip
    }
}
