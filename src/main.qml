import QtQuick 2.6
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2 as Dialogs
import QtQuick.Window 2.0

import test.logs 1.0

import ui 1.0
import ui.adapters 1.0

Main {
    id: root

    property var adapterUI: GenericAdapterUI {
        table: root.table
        model: events
        root: root
    }

    loaded: events.loaded

    onOpenFile: {
        fileDialog.open()
    }

    onFilter: {
        if(events.loaded) {
            filterDialog.open()
        }
    }

    onSetStartTime: {
        events.startTime = time
    }

    onSetEndTime: {
        events.endTime = time
    }

    onSetFilter: {
        events.filter = filter
    }

    onApplyFilter: {
        events.applyFilter()
        table.selection.clear()
    }

    onResetFilter: {
        events.resetFilter()
        context.filterRole = ""
    }

    onClearFilterOptions: {
        events.clearFilterOptions()
        context.filterRole = ""
    }

    onSearch: {
        events.search(text, regexp, caseSensitive)
    }

    onResetSearch: {
        events.resetSearch()
    }

    onSettings: {
        settingsDialog.open()
    }

    ApacheErrorLogAdapter {
        id: adapter
    }

    Dialogs.FileDialog {
        id: fileDialog
        nameFilters: [
            "Лог-файл (*.*log)",
            "Все файлы (*)"
        ]

        folder: Qt.resolvedUrl("./../")

        onFileUrlChanged: {
            events.filePath = fileUrl
        }
    }

    EventListModel {
        id: events
        filePath: ":/logs/apache_error.log"

        onFileError: {
            console.log(error)
        }

        onRowsInserted: {
            if(table.selection.count > 0) {
                details.opened = true

                table.selection.forEach(function(row) {
                    details.model = events.rowModel(row)
                    details.selectedRow = events.rowId(row)
                })
            }
        }

        adapter: adapter

        sortOrder: table.sortIndicatorOrder
        sortField: table.getColumn(table.sortIndicatorColumn).role

        onAdapterChanged: {
            UILayout.forAdapter(root.table, events, root, function(ui) {
                adapterUI = ui
            })
        }
    }

    FilterDialog {
        id: filterDialog

        table: root.table
        model: events

        onApply: {
            root.clearFilterOptions()

            root.setFilter(filter)

            if(startTime) {
                root.setStartTime(startTime)
            }

            if(startTime) {
                root.setEndTime(endTime)
            }

            root.applyFilter()
        }

        onReset: {
            root.resetFilter()
        }
    }

    SettingsDialog {
        id: settingsDialog
    }

    table {
        columns: adapterUI.layout
        rowDelegate: adapterUI.rowDelegate

        model: events
        adapter: events.adapter
        searchOccurrencies: events.occurrencies

        selection {
            onSelectionChanged: {
                if(table.selection.count > 0) {
                    details.opened = true

                    table.selection.forEach(function(row) {
                        details.model = events.rowModel(row)
                        details.selectedRow = events.rowId(row)
                    })
                } else {
                    details.opened = false
                }
            }
        }

        onDropped: {
            events.filePath = url
        }
    }

    header {
        filtered: events.filtered
        searched: events.searched
        occurrenciesCount: events.occurrenciesCount
    }

    statusBar {
        fileName: events.filePath
        adapterName: events.adapter.name
        recordCount: events.recordCount
        lineCount: events.lineCount
        progress: events.progress

        onStop: {
            events.cancelWorker()
        }

        onRefresh: {
            events.reload()
        }
    }
}
