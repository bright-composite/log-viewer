#ifndef ESCAPE_H
#define ESCAPE_H

#include <QByteArray>

inline QByteArray & unescape(QByteArray & s) {
	return s
		.replace("\\n", "\n")
		.replace("\\t", "\t")
		.replace("\\\"", "\"")
		.replace("\\\\", "\\");
}

inline QByteArray & escape(QByteArray & s) {
	return s
		.replace("\\", "\\\\")
		.replace("\n", "\\n")
		.replace("\t", "\\t")
		.replace("\"", "\\\"");
}

inline QByteArray && unescape(QByteArray && s) {
	return std::move(s
		.replace("\\n", "\n")
		.replace("\\t", "\t")
		.replace("\\\"", "\"")
		.replace("\\\\", "\\"));
}

inline QByteArray && escape(QByteArray && s) {
	return std::move(s
		.replace("\\", "\\\\")
		.replace("\n", "\\n")
		.replace("\t", "\\t")
		.replace("\"", "\\\""));
}

inline QString & unescape(QString & s) {
	return s
		.replace("\\n", "\n")
		.replace("\\t", "\t")
		.replace("\\\"", "\"")
		.replace("\\\\", "\\");
}

inline QString & escape(QString & s) {
	return s
		.replace("\\", "\\\\")
		.replace("\n", "\\n")
		.replace("\t", "\\t")
		.replace("\"", "\\\"");
}

inline QString unescape(const QString & s) {
	return QString(s)
		.replace("\\n", "\n")
		.replace("\\t", "\t")
		.replace("\\\"", "\"")
		.replace("\\\\", "\\");
}

inline QString escape(const QString & s) {
	return QString(s)
		.replace("\\", "\\\\")
		.replace("\n", "\\n")
		.replace("\t", "\\t")
		.replace("\"", "\\\"");
}

inline QString && unescape(QString && s) {
	return std::move(s
		.replace("\\n", "\n")
		.replace("\\t", "\t")
		.replace("\\\"", "\"")
		.replace("\\\\", "\\"));
}

inline QString && escape(QString && s) {
	return std::move(s
		.replace("\\", "\\\\")
		.replace("\n", "\\n")
		.replace("\t", "\\t")
		.replace("\"", "\\\""));
}

#endif // ESCAPE_H
