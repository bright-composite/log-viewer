#ifndef SORTFILTEROPTIONS_H
#define SORTFILTEROPTIONS_H

#include <QDateTime>
#include <QMap>
#include <adapters/ModelAdapter.h>

struct FilterEntry {
	QString expr;
	QRegExp::PatternSyntax syntax;
	Qt::CaseSensitivity sensitivity;
};

struct RawSortFilterOptions
{
	RawSortFilterOptions() :
		startTime(QDateTime::fromString("")),
		endTime(QDateTime::fromString("")),
		sortOrder(Qt::AscendingOrder)
	{}

	void clear() {
		*this = RawSortFilterOptions{};
	}

	QVariantMap mapFilter() const {
		QVariantMap out;

		for(auto i = filter.begin(); i != filter.end(); ++i) {
			auto & entry = i.value();
			out[QString::fromLatin1(i.key())] = QVariant::fromValue(QVariantMap {
				{"expr", entry.expr},
				{"syntax", entry.syntax}
			});
		}

		return out;
	}

	void unmapFilter(const QVariantMap & map) {
		filter.clear();

		for(auto i = map.begin(); i != map.end(); ++i) {
			auto entry = i.value().toMap();

			QVariant & syntax = entry["syntax"];
			QVariant & sensitivity = entry["sensitivity"];

			filter[i.key().toLatin1()] = FilterEntry{
				entry["expr"].toString(),
				syntax.isValid() ? QRegExp::PatternSyntax(syntax.toInt()) : QRegExp::Wildcard,
				sensitivity.isValid() ? Qt::CaseSensitivity(sensitivity.toInt()) : Qt::CaseSensitive
			};
		}
	}

	QDateTime startTime;
	QDateTime endTime;
	QByteArray sortField;
	Qt::SortOrder sortOrder;
	QMap<QByteArray, FilterEntry> filter;
};

struct SortFilterOptions
{
	SortFilterOptions() :
		startTime(QDateTime::fromString("")),
		endTime(QDateTime::fromString(""))
	{}

	SortFilterOptions(const SortFilterOptions & options) :
		startTime(options.startTime),
		endTime(options.endTime),
		sortRole(options.sortRole),
		sortOrder(options.sortOrder),
		filter(options.filter)
	{}

	SortFilterOptions(ModelAdapter * adapter, const RawSortFilterOptions & options) {
		startTime = options.startTime;
		endTime = options.endTime;
		sortRole = adapter->role(options.sortField);
		sortOrder = options.sortOrder;

		for(auto i = options.filter.begin(); i != options.filter.end(); ++i) {
			auto & entry = filter[adapter->role(i.key())];

			if(i.value().syntax == QRegExp::Wildcard || i.value().syntax == QRegExp::FixedString) {
				entry = i.value();
				entry.expr = QString(i.value().expr).replace("  ", " ");
			} else {
				entry = i.value();
			}
		}
	}

	~SortFilterOptions() {}

	QDateTime startTime;
	QDateTime endTime;
	int sortRole;
	Qt::SortOrder sortOrder;
	QMap<int, FilterEntry> filter;
};

Q_DECLARE_METATYPE(SortFilterOptions)

#endif // SORTFILTEROPTIONS_H
