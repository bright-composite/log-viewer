#ifndef MODELPATCH_H
#define MODELPATCH_H

#include <QObject>
#include <QVector>

class ModelPatch
{
public:
	ModelPatch() {}

	ModelPatch(const ModelPatch & patch) : _entries(patch._entries) {}

	ModelPatch(int index, int rowId) {
		_entries.append({index, rowId});
	}

	void add(int index, int rowId) {
		_entries.append({index, rowId});
	}

	void add(int index) {
		_entries.append({index, index});
	}

	int size() const {
		return _entries.size();
	}

	const QVector<QPair<int, int>> & entries() const {
		return _entries;
	}

	void clear() {
		_entries.clear();
	}

private:
	QVector<QPair<int, int>> _entries;
};

Q_DECLARE_METATYPE(ModelPatch)

#endif // MODELPATCH_H
