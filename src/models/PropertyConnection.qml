import QtQuick 2.0

/**
 *  The PropertyConnection component
 *  Allows automatic management of bindings when more then two properties are bound
 *
 *  If `target` is not specified explicitly, `target` will point to the parent component
 *  If `source` is not specified explicitly, `target` and `source` will point to the same component
 *  If `targetProperty` and `sourceProperty` are same, the `property` may be specified instead
 *
 *  `map` function can be defined to describe a transformation from the source property to the target property
 *  `unmap` function can be defined to describe a transformation from the target property to the source property
 *
 *  Example:
 *      ...
 *
 *      target: item
 *
 *      targetProperty: "date"
 *
 *      function map(value) {
 *          return DateTime.parse(value)
 *      }
 *
 *      sourceProperty: "text"
 *
 *      function unmap(value) {
 *          return DateTime.stringify(value)
 *      }
 *
 *      ...
 *
 */
Item {
    id: connection

    property var target: parent
    property var source: target
    property string property
    property string targetProperty: connection.property
    property string sourceProperty: connection.property

    property QtObject d: QtObject {
        // Prevents the occurrence of binding loops
        property bool changing: false
    }

    Component.onCompleted: {
        if(!target) {
            throw "Target must be specified!"
        }

        if(!targetProperty) {
            throw "Target property must be specified!"
        }

        if(!target.hasOwnProperty(targetProperty)) {
            throw "Property `" + targetProperty + "` of target " + target + " doesn't exist!"
        }

        if(!target[targetProperty + "Changed"]) {
            throw "Property `" + targetProperty + "` of target " + target + " is not notifiable!"
        }

        target[targetProperty + "Changed"].connect(function() {
            if(!d.changing) {
                d.changing = true
                source[sourceProperty] = connection.unmap ? connection.unmap(target[targetProperty]) : target[targetProperty]
                d.changing = false
            }
        })

        source[sourceProperty + "Changed"].connect(function() {
            if(!d.changing) {
                d.changing = true
                target[targetProperty] = connection.map ? connection.map(source[sourceProperty]) : source[sourceProperty]
                d.changing = false
            }
        })
    }
}
