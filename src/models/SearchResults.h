#ifndef SEARCHRESULTS_H
#define SEARCHRESULTS_H

#include <QObject>
#include <QVector>
#include <QVariantMap>

struct SearchResult {
	int row;
	int role;
	int start;
	int end;

	QVariant toVariant() const {
		QVariantMap out;

		out["row"] = row;
		out["role"] = role;
		out["start"] = start;
		out["end"] = end;

		return QVariant::fromValue(out);
	}
};

class SearchResults
{
public:
	SearchResults() {}
	SearchResults(const SearchResults & results) : _entries(results._entries) {}
	~SearchResults() {}

	void add(int row, int role, int start, int end) {
		_entries.append({row, role, start, end});
	}

	void add(const SearchResults & results) {
		_entries.append(results.entries());
	}

	int size() const {
		return _entries.size();
	}

	const QVector<SearchResult> & entries() const {
		return _entries;
	}

	void clear() {
		_entries.clear();
	}

private:
	QVector<SearchResult> _entries;
};

Q_DECLARE_METATYPE(SearchResults)

#endif // SEARCHRESULTS_H
