#ifndef EVENTLISTMODEL_H
#define EVENTLISTMODEL_H

#include <QAbstractTableModel>
#include <QFile>
#include <QDateTime>
#include <QList>
#include <QSet>
#include <QHash>

#include <adapters/ModelAdapter.h>

#include <models/SortFilterOptions.h>
#include <models/SearchResults.h>
#include <models/ModelPatch.h>

class EventListModel : public QAbstractListModel
{
	Q_OBJECT

	Q_PROPERTY(QString filePath READ filePath WRITE setFilePath NOTIFY filePathChanged)
	Q_PROPERTY(ModelAdapter * adapter READ adapter WRITE setAdapter NOTIFY adapterChanged)

	Q_PROPERTY(QByteArray sortField READ sortField WRITE setSortField NOTIFY sortFieldChanged)
	Q_PROPERTY(Qt::SortOrder sortOrder READ sortOrder WRITE setSortOrder NOTIFY sortOrderChanged)

	Q_PROPERTY(QVariantMap filter READ filter WRITE setFilter NOTIFY filterChanged)
	Q_PROPERTY(QDateTime startTime READ startTime WRITE setStartTime NOTIFY startTimeChanged)
	Q_PROPERTY(QDateTime endTime READ endTime WRITE setEndTime NOTIFY endTimeChanged)
	Q_PROPERTY(QDateTime defaultStartTime READ defaultStartTime NOTIFY sourceLoaded)
	Q_PROPERTY(QDateTime defaultEndTime READ defaultEndTime NOTIFY sourceLoaded)

	Q_PROPERTY(QVariantMap occurrencies READ occurrencies NOTIFY occurrenciesChanged)
	Q_PROPERTY(int occurrenciesCount READ occurrenciesCount NOTIFY occurrenciesChanged)

	Q_PROPERTY(bool filtered READ filtered NOTIFY filteredChanged)
	Q_PROPERTY(bool searched READ searched NOTIFY searchedChanged)

	Q_PROPERTY(int recordCount READ recordCount NOTIFY progressChanged)
	Q_PROPERTY(int lineCount READ lineCount NOTIFY progressChanged)
	Q_PROPERTY(bool loaded READ loaded NOTIFY loadedChanged)

	Q_PROPERTY(float progress READ progress NOTIFY progressChanged)

public:
	enum FileError {
		UrlError,
		NotFoundError,
		NotLocalFileError,
		ReadAccessError,
		ParseError
	}; Q_ENUM(FileError)

	enum PatternSyntax {
		RegExp = QRegExp::RegExp,
		RegExp2 = QRegExp::RegExp2,
		FixedString = QRegExp::FixedString,
		Wildcard = QRegExp::Wildcard
	}; Q_ENUM(PatternSyntax)

	explicit EventListModel(QObject *parent = 0);

	ModelAdapter * adapter() const;
	const QString & filePath() const;

	int recordCount() const;
	int lineCount() const;
	bool loaded() const;

	QVariantMap filter() const;
	QByteArray sortField() const;
	Qt::SortOrder sortOrder() const;
	QDateTime startTime() const;
	QDateTime endTime() const;
	QDateTime defaultStartTime() const;
	QDateTime defaultEndTime() const;

	bool filtered() const;
	bool searched() const;

	const RawSortFilterOptions & options() const;

	float progress() const;

	const QVariantMap & occurrencies() const;
	int occurrenciesCount() const;

	void setAdapter(ModelAdapter * adapter);
	void setFilePath(const QString &);

	void setSortField(const QByteArray & field);
	void setSortOrder(Qt::SortOrder order);
	void setFilter(const QVariantMap & map);
	void setStartTime(const QDateTime & time);
	void setEndTime(const QDateTime & time);

	virtual int rowCount(const QModelIndex & parent = QModelIndex()) const override;
	virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;

	virtual QHash<int, QByteArray> roleNames() const override;

signals:
	void filePathChanged();
	void adapterChanged();
	void sourceChanged(ModelSource * source);
	void sourceLoaded();
	void defaultsUpdated();

	void filterChanged();
	void sortFieldChanged();
	void sortOrderChanged();
	void startTimeChanged();
	void endTimeChanged();
	void filteredChanged();
	void searchedChanged();

	void reduceTriggered(const SortFilterOptions & options);
	void searchTriggered(const QString & text, bool regexp, bool caseSensitive);

	void occurrenciesChanged();

	void fileError(FileError error);

	void progressChanged();
	void loadedChanged();
	void canceled();

public slots:
	int role(const QString & name) const;
	int column(const QString & name) const;
	QVariant value(int row, int role) const;
	QVariant rowModel(int row) const;
	int rowId(int row) const;

	QStringList getDefaults(const QString & role) const;

	/**
	 * @brief reload
	 * Fully reload model source
	 */
	void reload();

	/**
	 * @brief refresh
	 * Clears model
	 */
	void clear();

	/**
	 * @brief applyFilter
	 * Applies the filter options and the time range
	 */
	void applyFilter();

	/**
	 * @brief resetFilter
	 * Clears filter options and refreshes the model
	 */
	void resetFilter();

	/**
	 * @brief clearFilterOptions
	 * Clears filter options without refreshing the model
	 */
	void clearFilterOptions();

	/**
	 * @brief search
	 * Starts search
	 *
	 * @param text
	 * @param regexp
	 * @param caseSensitive
	 */
	void search(const QString & text, bool regexp, bool caseSensitive);

	/**
	 * @brief resetSearch
	 * Clears search results
	 */
	void resetSearch();

	/**
	 * @brief applyPatch
	 * Called by worker, inserts new piece of the rows data
	 *
	 * @param patch
	 */
	void applyPatch(const ModelPatch & patch);

	/**
	 * @brief processResults
	 * Called by worker, adds new piece of the search results
	 *
	 * @param results
	 */
	void processResults(const SearchResults & results);

	/**
	 * @brief setProgress
	 * Called by worker, updates displayed progress
	 *
	 * @param progress
	 */
	void setProgress(float progress);

	/**
	 * @brief cancelWorker
	 * Cancels current worker job
	 */
	void cancelWorker();

private:
	/**
	 * @brief refresh
	 * Refreshes the model
	 */
	void refresh();

	QString _filePath;
	ModelAdapter * _adapter = nullptr;
	ModelSource * _source = nullptr;

	QVector<int> _indices;
	float _progress = 0.0f;
	bool _loaded = false;

	bool _filtered = false;
	bool _searched = false;
	RawSortFilterOptions _reduceOptions;
	QVariantMap _occurencies;
	int _occurenciesCount = 0;
};

#endif // EVENTLISTMODEL_H
