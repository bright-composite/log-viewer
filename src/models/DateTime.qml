pragma Singleton

import QtQuick 2.0

QtObject {
    id: dt

    property var locale: Qt.locale()

    function isDateTime(a) {
        return a && typeof a.getFullYear == "function"
    }

    /**
     *  Extracts date if `a` is date time
     *  Otherwise, `a` is the year, `b` is the month and `c` is the day of a month
     *  Default values are `1970, 0, 1`
     */
    function date(a, b, c) {
        if(isDateTime(a)) {
            return {year: a.getFullYear(), month: a.getMonth(), day: a.getDate()}
        }

        return {year: a || 1970, month: b || 0, day: c || 1}
    }

    /**
     *  Extracts time if `a` is date time
     *  Otherwise, `a` is the hour, `b` is the minute, `c` is the second, `d` is the millisecond
     *  Default values are `0, 0, 0, 0`
     */
    function time(a, b, c, d) {
        if(isDateTime(a)) {
            return {hour: a.getHours(), minute: a.getMinutes(), second: a.getSeconds(), millisecond: a.getMilliseconds()}
        }

        return {hour: a || 0, minute: b || 0, second: c || 0, millisecond: d || 0}
    }

    function dateTime(dateObj, timeObj) {
        if(dateObj && timeObj) {
            return new Date(dateObj.year, dateObj.month, dateObj.day, timeObj.hour, timeObj.minute, timeObj.second, timeObj.millisecond)
        }

        if(dateObj) {
            return new Date(dateObj.year, dateObj.month, dateObj.day)
        }

        if(timeObj) {
            return new Date(1970, 0, 1, timeObj.hour, timeObj.minute, timeObj.second, timeObj.millisecond)
        }

        return new Date(1970, 0, 1)
    }

    function dateFromString(s) {
        return date(dateTimeFromDateString(s))
    }

    function timeFromString(s) {
        return time(dateTimeFromTimeString(s))
    }

    function dateTimeFromString(s) {
        return Date.fromLocaleString(locale, s, "yyyy/MM/dd HH:mm:ss")
    }

    function fullDateTimeFromString(s) {
        return Date.fromLocaleString(locale, s, "yyyy/MM/dd HH:mm:ss.zzz")
    }

    function dateTimeFromDateString(s) {
        return Date.fromLocaleString(locale, s, "yyyy/MM/dd")
    }

    function dateTimeFromTimeString(s) {
        return Date.fromLocaleString(locale, s, "HH:mm:ss")
    }

    function dateToString(dateObj) {
        return dateTime(dateObj, undefined).toLocaleString(locale, "yyyy/MM/dd")
    }

    function timeToString(timeObj) {
        return dateTime(undefined, timeObj).toLocaleString(locale, "HH:mm:ss")
    }

    function dateTimeToString(dateTime) {
        return dateTime.toLocaleString(locale, "yyyy/MM/dd HH:mm:ss")
    }

    function fullDateTimeToString(dateTime) {
        return dateTime.toLocaleString(locale, "yyyy/MM/dd HH:mm:ss.zzz")
    }

    function dateTimeToDateString(dateTime) {
        return dateTime.toLocaleString(locale, "yyyy/MM/dd")
    }

    function dateTimeToTimeString(dateTime) {
        return dateTime.toLocaleString(locale, "HH:mm:ss")
    }

    /**
     *  Returns full string representation of date time
     */
    function stringify(dateTime) {
        return dateTime.toLocaleString(locale, "yyyy/MM/dd HH:mm:ss.zzz")
    }

    /**
     *  Expects full string representation of date time
     */
    function parse(s) {
        return Date.fromLocaleString(locale, s, "yyyy/MM/dd HH:mm:ss.zzz")
    }
}
