#include <models/EventListModel.h>

#include <QFileInfo>
#include <QUrl>
#include <QDebug>
#include <QFutureWatcher>
#include <QThread>

#include <adapters/ApacheErrorLogAdapter.h>
#include <services/AsyncModelWorker.h>
#include <services/ModelSource.h>

EventListModel::EventListModel(QObject *parent) : QAbstractListModel(parent) {
	AsyncModelWorker::launch(this);

	connect(this, &EventListModel::sourceLoaded, [this]() {
		qDebug() << "Loaded";
		_loaded = true;
		emit loadedChanged();
		emit defaultsUpdated();
	});
}

ModelAdapter * EventListModel::adapter() const {
	return _adapter;
}

const QString & EventListModel::filePath() const {
	return _filePath;
}

int EventListModel::recordCount() const {
	return _source ? _source->recordCount() : 0;
}

int EventListModel::lineCount() const {
	return _source ? _source->lineCount() : 0;
}

bool EventListModel::loaded() const {
	return _loaded;
}

int EventListModel::rowCount(const QModelIndex &) const {
	return _indices.size();
}

QVariant EventListModel::data(const QModelIndex &index, int role) const {
	if (!index.isValid()) {
		qWarning() << "Index is invalid";
		return {};
	}

	return value(index.row(), role);
}

QHash<int, QByteArray> EventListModel::roleNames() const {
	QHash<int, QByteArray> roles;

	if(_adapter == nullptr) {
		qWarning() << "No adapter selected";
		return roles;
	}

	for(int i = 0; i < _adapter->fieldCount(); ++i) {
		roles[ModelAdapter::StartRole + i] = _adapter->field(ModelAdapter::StartRole + i);
	}

	return roles;
}

int EventListModel::role(const QString & name) const {
	if(_adapter == nullptr) {
		qWarning() << "No adapter selected";
		return -1;
	}

	return _adapter->role(name);
}

int EventListModel::column(const QString & name) const {
	if(_adapter == nullptr) {
		qWarning() << "No adapter selected";
		return -1;
	}

	for(int i = 0; i < _adapter->fieldCount(); ++i) {
		if(_adapter->field(ModelAdapter::StartRole + i) == name.toUtf8()) {
			return i;
		}
	}

	return -1;
}

QVariant EventListModel::value(int row, int role) const {
	if(_source == nullptr) {
		qWarning() << "No source selected";
		return {};
	}

	if (row < 0 || row >= _indices.size()) {
		qWarning() << "Unknown row" << row;
		return {};
	}

	if (role < ModelAdapter::StartRole || role > ModelAdapter::StartRole + _adapter->fieldCount()) {
		qWarning() << "Unknown role:" << role;
		return {};
	}

	try {
		return _source->getRow(_indices[row])[role];
	} catch(const RecordParseException & e) {
		qWarning() << "Error:" << "Could not parse " + e.object() + " at line " + QString::number(e.line()) + " (" + _source->filePath() + ")";
		return {};
	}
}

QVariant EventListModel::rowModel(int row) const {
	if(_source == nullptr) {
		qWarning() << "No source selected";
		return {};
	}

	if (row < 0 || row >= _indices.size()) {
		qWarning() << "Unknown row:" << row;
		return {};
	}

	try {
		auto model = _source->getRow(_indices[row]);

		QVariantMap out;

		for(auto i = model.begin(); i != model.end(); ++i) {
			out[_adapter->field(i.key())] = i.value();
		}

		return QVariant::fromValue(out);
	} catch(const RecordParseException & e) {
		qWarning() << "Error:" << "Could not parse " + e.object() + " at line " + QString::number(e.line()) + " (" + _filePath + ")";
		return {};
	}
}

int EventListModel::rowId(int row) const {
	if(_source == nullptr) {
		qWarning() << "No source selected";
		return -1;
	}

	if (row < 0 || row >= _indices.size()) {
		return -1;
	}

	return _indices[row];
}

QStringList EventListModel::getDefaults(const QString & role) const {
	if(_source == nullptr) {
		qWarning() << "No source selected";
		return {};
	}

	return _source->getDefaults(this->role(role));
}

const RawSortFilterOptions & EventListModel::options() const {
	return _reduceOptions;
}

QVariantMap EventListModel::filter() const {
	return _reduceOptions.mapFilter();
}

QByteArray EventListModel::sortField() const {
	return _reduceOptions.sortField;
}

Qt::SortOrder EventListModel::sortOrder() const {
	return _reduceOptions.sortOrder;
}

QDateTime EventListModel::startTime() const {
	return _reduceOptions.startTime;
}

QDateTime EventListModel::endTime() const {
	return _reduceOptions.endTime;
}

QDateTime EventListModel::defaultStartTime() const {
	return _source ? _source->startTime() : QDateTime::fromString("");
}

QDateTime EventListModel::defaultEndTime() const {
	return _source ? _source->endTime() : QDateTime::fromString("");
}

bool EventListModel::filtered() const {
	return _filtered;
}

bool EventListModel::searched() const {
	return _searched;
}

float EventListModel::progress() const {
	return _progress;
}

const QVariantMap & EventListModel::occurrencies() const {
	return _occurencies;
}

int EventListModel::occurrenciesCount() const {
	return _occurenciesCount;
}

void EventListModel::setAdapter(ModelAdapter * adapter) {
	if(adapter == nullptr) {
		return;
	}

	_adapter = adapter;
	emit adapterChanged();

	setFilePath(_filePath);
}

void EventListModel::setFilePath(const QString & path) {
	if (_source) {
		_source->deleteLater();
		_source = nullptr;
	}

	_occurencies.clear();
	clearFilterOptions();
	emit progressChanged();

	_loaded = false;
	emit loadedChanged();

	QUrl url(path);

	if(url.isValid()) {
		_filePath = url.toLocalFile();
	} else {
		_filePath = path;
	}

	QFile file(_filePath);

	qDebug() << "New file path:" << _filePath;

	if(!file.exists()) {
		if(!url.isValid()) {
			qWarning() << path << "is not a valid file url";
			emit fileError(UrlError);
			_filePath = "";
		} else if(!url.isLocalFile()) {
			qWarning() << path << "is not a local file";
			emit fileError(NotLocalFileError);
			_filePath = "";
		} else {
			qWarning() << "File" << file.fileName() << "not found";
			emit fileError(NotFoundError);
			_filePath = "";
		}
	} else {
		if(!file.open(QIODevice::ReadOnly)) {
			qWarning() << "Can't open file" << file.fileName() << "for reading";
			emit fileError(ReadAccessError);
			_filePath = "";
		}

		file.close();
	}

	emit filePathChanged();

	if(!_filePath.isEmpty() && _adapter) {
		_source = new ModelSource(_filePath, _adapter, this);
		connect(_source, SIGNAL(loaded()), this, SIGNAL(sourceLoaded()));
	}

	clearFilterOptions();
	_filtered = false;
	emit filteredChanged();

	resetSearch();

	emit sourceChanged(_source);
}

void EventListModel::setSortField(const QByteArray & field) {
	if(_reduceOptions.sortField != field) {
		_reduceOptions.sortField = field;
		emit sortFieldChanged();
		refresh();
	}
}

void EventListModel::setSortOrder(Qt::SortOrder order) {
	if(_reduceOptions.sortOrder != order) {
		_reduceOptions.sortOrder = order;
		emit sortOrderChanged();
		refresh();
	}
}

void EventListModel::setFilter(const QVariantMap & filter) {
	_reduceOptions.unmapFilter(filter);
	emit filterChanged();
}

void EventListModel::setStartTime(const QDateTime & time) {
	_reduceOptions.startTime = time;
	emit startTimeChanged();
}

void EventListModel::setEndTime(const QDateTime & time) {
	_reduceOptions.endTime = time;
	emit endTimeChanged();
}

/**
 * @brief search
 * Starts search
 *
 * @param text
 * @param regexp
 */
void EventListModel::search(const QString & text, bool regexp, bool caseSensitive) {
	if(_loaded) {
		_searched = true;
		_occurencies.clear();
		_occurenciesCount = 0;

		emit searchedChanged();
		emit occurrenciesChanged();
		emit searchTriggered(text, regexp, caseSensitive);
	}
}

/**
 * @brief resetSearch
 * Clears search results
 */
void EventListModel::resetSearch() {
	if(_searched) {
		_searched = false;
		_occurencies.clear();
		_occurenciesCount = 0;

		emit searchedChanged();
		emit occurrenciesChanged();
	}
}

void EventListModel::reload() {
	if(_source != nullptr) {
		_source->deleteLater();
		_source = nullptr;
	}

	if(!_filePath.isEmpty() && _adapter) {
		_source = new ModelSource(_filePath, _adapter, this);
		connect(_source, SIGNAL(loaded()), this, SIGNAL(sourceLoaded()));
	}

	clearFilterOptions();
	_filtered = false;
	emit filteredChanged();

	resetSearch();

	emit sourceChanged(_source);
}

/**
 * @brief refresh
 * Clears model
 */
void EventListModel::clear() {
	if(_indices.size() > 0) {
		qDebug() << "clear model";
		beginRemoveRows(QModelIndex(), 0, _indices.size() - 1);
		_indices.clear();
		endRemoveRows();
	}
}

/**
 * @brief refresh
 * Refreshes model
 */
void EventListModel::refresh() {
	if(_loaded) {
		emit reduceTriggered({_adapter, _reduceOptions});
	}
}

/**
 * @brief applyFilter
 * Applies the filter options and the time range
 */
void EventListModel::applyFilter() {
	if(_loaded) {
		refresh();
		_filtered = true;
		emit filteredChanged();
	}
}

/**
 * @brief resetFilter
 * Clears filter options and refreshes the model
 */
void EventListModel::resetFilter() {
	clearFilterOptions();

	if(_filtered) {
		refresh();
		_filtered = false;
		emit filteredChanged();
	}
}

/**
 * @brief resetFilter
 * Clears filter options and refreshes model
 */
void EventListModel::clearFilterOptions() {
	_reduceOptions.clear();

	emit filterChanged();
	emit startTimeChanged();
	emit endTimeChanged();
}

void EventListModel::applyPatch(const ModelPatch & patch) {
	qDebug() << "apply patch:" << patch.size() << "rows";

	for(auto & entry : patch.entries()) {
		beginInsertRows(QModelIndex(), entry.first, entry.first);
		_indices.insert(entry.first, entry.second);
		endInsertRows();
	}
}

void EventListModel::processResults(const SearchResults & results) {
	qDebug() << "Found" << results.size() << "occurrencies";

	for(const SearchResult & r : results.entries()) {
		auto & rowList = _occurencies[QString::number(r.row)];
		QVariantList list;

		if(rowList.isValid()) {
			list = rowList.toList();
		}

		list.append(r.toVariant());
		rowList = list;
	}

	_occurenciesCount += results.size();
	emit occurrenciesChanged();
}

void EventListModel::setProgress(float progress) {
	_progress = progress;
	emit progressChanged();
}

void EventListModel::cancelWorker() {
	emit canceled();
}
