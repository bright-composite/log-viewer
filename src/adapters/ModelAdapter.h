#ifndef MODELADAPTER_H
#define MODELADAPTER_H

#include <QObject>
#include <QVariantMap>
#include <QDateTime>
#include <QSet>

#include <exceptions/FieldParseException.h>
#include <exceptions/RecordParseException.h>

class ModelSource;

using ModelRow = QHash<int, QVariant>;

/**
 * @brief The ModelAdapter class
 * This class provides support for different types of logs
 * Adapters are accessed in different threads so they can not contain a mutable state
 */
class ModelAdapter : public QObject
{
	Q_OBJECT

	Q_PROPERTY(QString key READ key CONSTANT)
	Q_PROPERTY(QString name READ name CONSTANT)

public:
	enum Role {
		StartRole = Qt::UserRole + 1,
		LineNumber = StartRole,
		Severity,
		Time,
		Message,
		CustomRole
	};

	explicit ModelAdapter(QObject *parent = 0);

	/**
	 * @brief key
	 * @return Short string that helps to identify the adapter. Used by UILayout to obtain a custom adapter ui
	 */
	virtual QString key() const {
		return "Generic";
	}

	/**
	 * @brief name
	 * @return Display name of the adapter
	 */
	virtual QString name() const {
		return key();
	}

	/**
	 * @brief fieldCount
	 * @return Number of fields available for this adapter. Default value is 4 (line number, severity, time and message)
	 */
	virtual int fieldCount() const {
		return 4;
	}

	/**
	 * @brief value
	 * Obtains single value by eventIndex and field
	 *
	 * @param ModelSource * source
	 * @param int eventIndex
	 * @param int fieldId
	 *
	 * @return QVariant
	 *
	 * @throws RecordParseException
	 */
	virtual QVariant value(ModelSource *, int, int) const throw(RecordParseException) {
		return {};
	}

	/**
	 * @brief row
	 * Obtains the whole record by its index
	 *
	 * @param ModelSource * source
	 * @param int eventIndex
	 *
	 * @return ModelRow
	 *
	 * @throws RecordParseException
	 */
	virtual ModelRow row(ModelSource *, int) const throw(RecordParseException) {
		return {};
	}

	/**
	 * @brief nextRecordStart
	 * Seek for the next record in block
	 *
	 * @param QByteArray block
	 *
	 * @return int - the offset of the found record or -1
	 */
	virtual int nextRecordStart(const QByteArray &) const throw() {
		return -1;
	}

	/**
	 * @brief processRecord
	 * Read event and collect defaults
	 *
	 * @param ModelSource * source
	 * @param QByteArray bytes
	 */
	virtual void processRecord(ModelSource *, const QByteArray &) const throw() {}

	static const QString SEVERITY_INFO;
	static const QString SEVERITY_NOTICE;
	static const QString SEVERITY_WARNING;
	static const QString SEVERITY_ERROR;

	static const QStringList SEVERITIES;

public slots:
	/**
	 * @brief field
	 * @param role
	 * @return QByteArray - name of the field in the event structure for the given role
	 */
	virtual QByteArray field(int) const throw() {
		return "";
	}

	/**
	 * @brief role
	 * @param name
	 * @return int - id of role for the given field name
	 */
	int role(const QString & name) const throw();
};

#endif // MODELADAPTER_H
