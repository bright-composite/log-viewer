#ifndef APACHEERRORLOGADAPTER_H
#define APACHEERRORLOGADAPTER_H

#include <adapters/ModelAdapter.h>
#include <QDateTime>
#include <QSet>

/**
 * @brief The ApacheErrorLogAdapter class
 * Provides basic support for error logs from Apache server
 */
class ApacheErrorLogAdapter : public ModelAdapter
{
	Q_OBJECT
public:
	enum Field {
		Unit = CustomRole,
		Level
	};

	struct Event
	{
		QDateTime time;
		QString unit;
		QString level;
		QString message;
	};

	explicit ApacheErrorLogAdapter(QObject *parent = 0);

	virtual QString key() const override {
		return "ApacheErrorLog";
	}

	virtual QString name() const override {
		return "Apache server error log";
	}

	virtual int fieldCount() const override {
		return 6;
	}

	virtual QByteArray field(int role) const throw() override;

	virtual QVariant value(ModelSource * source, int eventIndex, int fieldId) const throw(RecordParseException) override;

	virtual ModelRow row(ModelSource * source, int eventIndex) const throw(RecordParseException) override;

	virtual int nextRecordStart(const QByteArray & block) const throw() override;

	virtual void processRecord(ModelSource * source, const QByteArray & bytes) const throw() override;

private:
	/**
	 * @brief transform
	 * Creates generic hash map from the event structure
	 *
	 * @param source
	 * @param event
	 * @param eventIndex
	 * @return ModelRow
	 */
	ModelRow transform(ModelSource * source, const Event & event, int eventIndex) const throw();

	/**
	 * @brief readEvent
	 * Parses raw bytes to event fields
	 *
	 * @param bytes
	 * @return Event
	 *
	 * @throws FieldParseException
	 */
	Event readEvent(const QByteArray & bytes) const throw(FieldParseException);

	/**
	 * @brief obtainEvent
	 * Parses the whole event record obtained from the given source by event index
	 * Also transforms the FieldParseException to the more informative RecordParseException
	 *
	 * @param source
	 * @param eventIndex
	 *
	 * @return Event
	 *
	 * @throws RecordParseException
	 */
	Event obtainEvent(ModelSource * source, int eventIndex) const throw(RecordParseException);
};

#endif // APACHEERRORLOGADAPTER_H
