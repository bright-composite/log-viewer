#include "ModelAdapter.h"

#include <QDebug>

const QString ModelAdapter::SEVERITY_INFO = QStringLiteral("info");
const QString ModelAdapter::SEVERITY_NOTICE = QStringLiteral("notice");
const QString ModelAdapter::SEVERITY_WARNING = QStringLiteral("warning");
const QString ModelAdapter::SEVERITY_ERROR = QStringLiteral("error");

const QStringList ModelAdapter::SEVERITIES = QStringList()
		<< ModelAdapter::SEVERITY_INFO
		<< ModelAdapter::SEVERITY_NOTICE
		<< ModelAdapter::SEVERITY_WARNING
		<< ModelAdapter::SEVERITY_ERROR;

ModelAdapter::ModelAdapter(QObject *parent) : QObject(parent) {}

int ModelAdapter::role(const QString & name) const throw() {
	for(int i = 0; i < fieldCount(); ++i) {
		if(field(ModelAdapter::StartRole + i) == name.toUtf8()) {
			return ModelAdapter::StartRole + i;
		}
	}

	return -1;
}
