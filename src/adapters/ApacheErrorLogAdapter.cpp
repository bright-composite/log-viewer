#include "ApacheErrorLogAdapter.h"
#include <regex>

#include <QDebug>

#include <utils/Escape.h>
#include <services/ModelSource.h>

ApacheErrorLogAdapter::ApacheErrorLogAdapter(QObject *parent) : ModelAdapter(parent) {}

/**
 * @brief nextRecordStart
 * Seek for the next record in block
 *
 * @param QByteArray block
 * @return int - the offset of the found record or -1
 */
int ApacheErrorLogAdapter::nextRecordStart(const QByteArray & block) const throw() {
	int i = block.indexOf("\n[");

	if(i < 0) {
		return i;
	}

	// Skip '\n' character
	return i + 1;
}

/**
 * @brief processRecord
 * Read event and collect defaults
 *
 * @param source
 * @param bytes
 */
void ApacheErrorLogAdapter::processRecord(ModelSource * source, const QByteArray & bytes) const throw()  {
	try {
		auto startTime = source->startTime();
		auto endTime = source->endTime();

		auto event = readEvent(bytes);

		if(!startTime.isValid() || event.time < startTime) {
			source->setStartTime(event.time);
		}

		if(!endTime.isValid() || event.time > endTime) {
			source->setEndTime(event.time);
		}

		source->addDefaults(Unit, event.unit);
		source->addDefaults(Level, event.level);
	} catch(const FieldParseException & e) {
		qWarning() << "Error:" << "Could not parse `" + e.field() + "` field";
	}
}

/**
 * @brief field
 * @param role
 * @return QByteArray - name of the field in the event structure for the given role
 */
QByteArray ApacheErrorLogAdapter::field(int role) const throw() {
	switch(role) {
		case LineNumber:
			return "line";
		case Severity:
			return "severity";
		case Time:
			return "time";
		case Unit:
			return "unit";
		case Level:
			return "level";
		case Message:
			return "message";
		default: return "";
	}
}

/**
 * @brief getSeverity
 * Groups apache and php error levels to the common categories
 *
 * @param event
 * @return QString
 */
static QString getSeverity(const ApacheErrorLogAdapter::Event & event) {
	if(event.unit.contains("php")) {
		if(event.level.contains("error") || event.level == "parse") {
			return ModelAdapter::SEVERITY_ERROR;
		}

		if(event.level.contains("warning") || event.level.contains("deprecated")) {
			return ModelAdapter::SEVERITY_WARNING;
		}

		if(event.level == "notice" || event.level == "strict") {
			return ModelAdapter::SEVERITY_NOTICE;
		}

		return ModelAdapter::SEVERITY_INFO;
	}

	if(event.level == "emerg" || event.level == "crit" || event.level == "alert" || event.level == "error") {
		return ModelAdapter::SEVERITY_ERROR;
	}

	if(event.level == "warn") {
		return ModelAdapter::SEVERITY_WARNING;
	}

	if(event.level == "notice") {
		return ModelAdapter::SEVERITY_NOTICE;
	}

	return ModelAdapter::SEVERITY_INFO;
}

/**
 * @brief value
 * Obtains single value by eventIndex and field
 *
 * @param ModelSource * source
 * @param int eventIndex
 * @param int fieldId
 *
 * @return QVariant
 *
 * @throws RecordParseException
 */
QVariant ApacheErrorLogAdapter::value(ModelSource * source, int eventIndex, int role) const throw(RecordParseException) {
	if(role ==  LineNumber) {
		return source->pointer(eventIndex).line;
	}

	auto event = obtainEvent(source, eventIndex);

	switch(role) {
		case Severity:
			return getSeverity(event);
		case Time:
			return event.time;
		case Unit:
			return event.unit;
		case Level:
			return event.level;
		case Message:
			return event.message;

		default: return "";
	}
}

/**
 * @brief row
 * Obtains the whole record by its index
 *
 * @param ModelSource * source
 * @param int eventIndex
 *
 * @return ModelRow
 *
 * @throws RecordParseException
 */
ModelRow ApacheErrorLogAdapter::row(ModelSource * source, int eventIndex) const throw(RecordParseException) {
	return transform(source, obtainEvent(source, eventIndex), eventIndex);
}

/**
 * @brief transform
 * Creates generic hash map from the event structure
 *
 * @param source
 * @param event
 * @param eventIndex
 * @return ModelRow
 */
ModelRow ApacheErrorLogAdapter::transform(ModelSource * source, const Event & event, int eventIndex) const throw() {
	ModelRow out;

	out[LineNumber] = source->pointer(eventIndex).line;
	out[Severity] = getSeverity(event);
	out[Time] = event.time;
	out[Unit] = event.unit;
	out[Level] = event.level;
	out[Message] = event.message;

	return out;
}

static QMap<QString, int> MONTH_MAP = ([]() {
	QMap<QString, int> out;

	out["Jan"] = 1;
	out["Feb"] = 2;
	out["Mar"] = 3;
	out["Apr"] = 4;
	out["May"] = 5;
	out["Jun"] = 6;
	out["Jul"] = 7;
	out["Aug"] = 8;
	out["Sep"] = 9;
	out["Oct"] = 10;
	out["Nov"] = 11;
	out["Dec"] = 12;

	return out;
})();

/**
 * @brief parseTime
 * @param record
 * @return
 */
static QDateTime parseTime(const std::string & record) {
	std::regex regex(R"(^\[(.+)\].*\[.+\].*\[.+\].*(\[.+\]).+$)");
	std::regex regex2(R"(^\[(.+)\].*\[.+\].*\[.+\].+$)");
	std::smatch m;

	if(!std::regex_match(record, m, regex)) {
		if(!std::regex_match(record, m, regex2)) {
			throw FieldParseException("time");
		}
	}

	if(m.size() < 2) {
		qWarning() << "Not enough matches";
		throw FieldParseException("time");
	}

	std::regex dateRegex(R"(^\w{3} (\w{3}) (\d{2}) (\d{2}):(\d{2}):(\d{2}).(\d{3})\d{3} (\d{4}))");
	auto dateString = m[1].str();

	if(!std::regex_match(dateString, m, dateRegex)) {
		qWarning() << "Invalid format";
		throw FieldParseException("time");
	}

	int year = QString::fromStdString(m[7].str()).toInt();
	int month = MONTH_MAP[QString::fromStdString(m[1].str())];
	int day = QString::fromStdString(m[2].str()).toInt();
	int hour = QString::fromStdString(m[3].str()).toInt();
	int minute = QString::fromStdString(m[4].str()).toInt();
	int second = QString::fromStdString(m[5].str()).toInt();
	int millisecond = QString::fromStdString(m[6].str()).toInt();

	return QDateTime(
		QDate(year, month, day),
		QTime(hour, minute, second, millisecond)
	);
}

/**
 * @brief parseUnit
 * @param record
 * @return
 */
static QString parseUnit(const std::string & record) {
	std::regex regex(R"(^\[.+\] \[(.*):.+\] \[.+\].*\[.+\].+$)");
	std::regex regex2(R"(^\[.+\] \[(.*):.+\] \[.+\].+$)");
	std::smatch m;

	if(!std::regex_match(record, m, regex)) {
		if(!std::regex_match(record, m, regex2)) {
			throw FieldParseException("unit");
		}
	}

	if(m.size() < 2) {
		qWarning() << "Not enough matches";
		throw FieldParseException("unit");
	}

	if(!m[1].str().empty()) {
		return QString::fromStdString(m[1].str());
	}

	std::regex phpregex(R"(^\[.+\] \[.+\] \[.+\].*(?:\[.+\])\s*PHP .+?:.+$)");
	std::regex phpregex2(R"(^\[.+\] \[.+\] \[.+\]\s*PHP .+?:.+$)");

	if(std::regex_match(record, phpregex) || std::regex_match(record, phpregex2)) {
		return "php";
	}

	return "";
}

/**
 * @brief parseLevel
 * @param record
 * @return
 */
static QString parseLevel(const std::string & record) {
	std::regex phpregex(R"(^\[.+\] \[.+\] \[.+\].*(?:\[.+\])\s*PHP (.+?):.+$)");
	std::regex phpregex2(R"(^\[.+\] \[.+\] \[.+\]\s*PHP (.+?):.+$)");
	std::smatch m;

	if((std::regex_match(record, m, phpregex) || std::regex_match(record, m, phpregex2)) && m.size() >= 2) {
		return QString::fromStdString(m[1].str()).toLower();
	}

	std::regex regex(R"(^\[.+\] \[.*:(.+)\] \[.+\].*(?:\[.+\]).+$)");
	std::regex regex2(R"(^\[.+\] \[.*:(.+)\] \[.+\].+$)");

	if(!std::regex_match(record, m, regex)) {
		if(!std::regex_match(record, m, regex2)) {
			throw FieldParseException("level");
		}
	}

	if(m.size() < 2) {
		qWarning() << "Not enough matches";
		throw FieldParseException("level");
	}

	return QString::fromStdString(m[1].str());
}

/**
 * @brief parseMessage
 * @param record
 * @return
 */
static QString parseMessage(const std::string & record) {
	std::regex regex(R"(^\[.+\] \[.+\] \[.+\].*(?:\[.+\])(.+)$)");
	std::regex regex2(R"(^\[.+\] \[.+\] \[.+\](.+)$)");
	std::smatch m;

	if(!std::regex_match(record, m, regex)) {
		if(!std::regex_match(record, m, regex2)) {
			throw FieldParseException("message");
		}
	}

	if(m.size() < 2) {
		qWarning() << "Not enough matches";
		throw FieldParseException("message");
	}

	return QString::fromUtf8(unescape(QByteArray::fromStdString(m[1].str()).trimmed()));
}

/**
 * @brief readEvent
 * Parses raw bytes to event fields
 *
 * @param bytes
 * @return Event
 *
 * @throws FieldParseException
 */
ApacheErrorLogAdapter::Event ApacheErrorLogAdapter::readEvent(const QByteArray & bytes) const throw(FieldParseException) {
	std::string record = escape(bytes.trimmed()).toStdString();
	Event event;

	event.time = parseTime(record);
	event.unit = parseUnit(record);
	event.level = parseLevel(record);
	event.message = unescape(parseMessage(record));

	return event;
}

/**
 * @brief obtainEvent
 * Parses the whole event record obtained from the given source by event index
 * Also transforms the FieldParseException to the more informative RecordParseException
 *
 * @param source
 * @param eventIndex
 *
 * @return Event
 *
 * @throws RecordParseException
 */
ApacheErrorLogAdapter::Event ApacheErrorLogAdapter::obtainEvent(ModelSource * source, int eventIndex) const throw(RecordParseException) {
	auto & pointer = source->pointer(eventIndex);
	qint64 length = source->pointer(eventIndex + 1).offset - pointer.offset;

	QByteArray bytes = source->read(pointer.offset, length);

	if (bytes.size() != length)
	   throw RecordParseException(pointer.line);

	try {
		return readEvent(bytes);
	} catch(const FieldParseException & e) {
		qDebug() << bytes;
		throw RecordParseException(pointer.line, "`" + e.field() + "` field");
	}
}
