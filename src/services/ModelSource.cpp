#include "ModelSource.h"

#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QTemporaryFile>
#include <QCoreApplication>

#include <services/AsyncModelWorker.h>

TempFileRef::TempFileRef(const QString & original) {
	QFileInfo info(original);

	if(info.isNativePath()) {
		_filePath = QDir::temp().filePath(info.fileName() + "_" + QDateTime::currentDateTime().toString(Qt::ISODate).replace(":", "-"));
		qDebug() << "Create temporary:" << _filePath;

		if(!QFile::copy(original, _filePath)) {
			qWarning() << "Can't copy" << original << "to" << _filePath;
		}
	} else {
		auto temp = QTemporaryFile::createNativeFile(original);
		auto tempName = temp->fileName();
		temp->setAutoRemove(false);
		temp->close();

		_filePath = tempName;
		qDebug() << "Create temporary:" << _filePath;

		temp->deleteLater();
	}

}

TempFileRef::~TempFileRef() {
	if(QFile::remove(_filePath)) {
		qDebug() << "Removed temporary:" << _filePath;
	} else {
		qWarning() << "Can't remove temporary:" << _filePath;
	}
}

ModelSource::ModelSource(const QString & filePath, ModelAdapter * adapter, QObject * parent) :
	QObject(parent),
	_fileRef(TempFileRef::make(filePath)),
	_file(_fileRef->filePath()),
	_adapter(adapter),
	_lastOffset(0),
	_lastLine(1)
{
	if(!_file.open(QFile::ReadOnly)) {
		qWarning() << "Can't open file" << _file.fileName();
	}

	_pointers.push_back({_lastOffset, _lastLine});

	QFileInfo info(_file);
	_fileSize = info.size();
}

ModelSource::ModelSource(ModelSource * origin, QObject * parent) :
	QObject(parent),
	_fileRef(origin->_fileRef),
	_file(_fileRef->filePath()),
	_adapter(origin->_adapter),
	_pointers(origin->_pointers),
	_lastOffset(origin->_lastOffset),
	_lastLine(origin->_lastLine),
	_fileSize(origin->_fileSize)
{
	if(!_file.open(QFile::ReadOnly)) {
		qWarning() << "Can't open file" << _file.fileName();
	}

	connect(this, SIGNAL(pointerInserted(FilePointer)), origin, SLOT(insertPointer(FilePointer)));
	connect(this, SIGNAL(defaultsUpdated(ModelDefaults)), origin, SLOT(updateDefaults(ModelDefaults)));
	connect(this, SIGNAL(loaded()), origin, SIGNAL(loaded()));
}

ModelSource::~ModelSource() {
	_file.close();
}

/**
 * @brief ModelSource::createLink
 * Creates a new instance of ModelSource and connects to it to receive updates
 *
 * @param parent
 * @return new ModelSource instance
 */
ModelSource * ModelSource::createLink(QObject * parent) {
	return new ModelSource(this, parent);
}

QString ModelSource::filePath() const {
	return _file.fileName();
}

bool ModelSource::canFetchMore() const {
	return _lastOffset < _fileSize;
}

/**
 * @brief getRow
 * @param int rowId
 * @return a row obtained from the file or the cache
 */
ModelRow ModelSource::getRow(int rowId) {
	auto i = _cache.find(rowId);

	if(i != _cache.end()) { // a row was found in the cache
		return i.value();
	}

	auto r = _adapter->row(this, rowId);

	if(_cache.size() > 256) { // clear old cached values to prevent cache overflow
		for(int i = 0; i < 32; ++i) {
			_cache.erase(_cache.begin());
		}

		for(int i = 0; i < 32; ++i) {
			_cache.erase(--_cache.end());
		}
	}

	return _cache.insert(rowId, r).value(); // insert an obtained row and return it
}

const FilePointer & ModelSource::pointer(int rowId) const {
	return _pointers[rowId];
}

QByteArray ModelSource::read(int length) {
	return _file.read(length);
}

QByteArray ModelSource::read(qint64 offset, int length) {
	_file.seek(offset);
	return _file.read(length);
}

void ModelSource::setChunkSize(int chunkSize) {
	_chunkSize = chunkSize;
}

void ModelSource::fetchMore() {
	if(_lastOffset > 0) {
		_file.seek(_lastOffset);
	}

	QByteArray block;
	ModelPatch patch;

	_canceled = false;
	_loaded = false;

	while (_lastOffset < _fileSize) {
		QCoreApplication::processEvents();

		if(_canceled) {
			qDebug() << "Loading has been cancelled, parsed" << _pointers.size() - 1 << "records and" << _lastLine - 1 << "lines";
			emit defaultsUpdated(_defaults);
			emit loaded();
			return;
		}

		auto bytes = read(_chunkSize);

		if (bytes.isEmpty())
			break;

		block += bytes;
		qint64 i = 0;

		while(true) {
			i = _adapter->nextRecordStart(block);

			if(i < 0) {
				break;
			}

			processBlock(patch, block.left(i));
			block = block.mid(i);
		}

		if(patch.size() >= 256) {
			emit fragmentObtained(patch, float(_lastOffset) / float(_fileSize));
			patch.clear();
		}
	}

	processBlock(patch, block);
	emit fragmentObtained(patch, float(_lastOffset) / float(_fileSize));
	patch.clear();

	qDebug() << "File parsed," << _pointers.size() - 1 << "records and" << _lastLine - 1 << "lines";

	emit defaultsUpdated(_defaults);

	_loaded = true;
	emit loaded();
}
/*
void ModelSource::fetchMore() {
	if(_lastOffset > 0) {
		_file.seek(_lastOffset);
	}

	ModelPatch patch;

	auto bytes = read(_chunkSize);

	if (bytes.isEmpty())
		return;

	_buffer += bytes;
	qint64 i = 0;

	while(true) {
		i = _adapter->nextRecordStart(_buffer);

		if(i < 0) {
			break;
		}

		processBlock(patch, _buffer.left(i));
		_buffer = _buffer.mid(i);
	}

	if(_lastOffset + _buffer.size() == _fileSize) {
		processBlock(patch, _buffer);
		_buffer.clear();
	}

	emit fragmentObtained(patch, float(_lastOffset) / float(size));
}
*/
void ModelSource::processBlock(ModelPatch & patch, const QByteArray & block) {
	_adapter->processRecord(this, block);

	_lastLine += block.count('\n');
	_lastOffset += block.size();

	patch.add(_pointers.size() - 1);

	FilePointer pointer{_lastOffset, _lastLine};
	_pointers.push_back(pointer);
	emit pointerInserted(pointer);
}

/**
 * @brief getDefaults
 * @param role
 * @return list of default values for the given role
 */
QStringList ModelSource::getDefaults(int role) const {
	if(role == ModelAdapter::Severity) {
		return ModelAdapter::SEVERITIES;
	}

	auto i = _defaults.other.find(role);

	if(i == _defaults.other.end()) {
		return {};
	}

	return i.value().toList();
}

/**
 * @brief addDefaults
 * Adds default value for the given role
 *
 * @param role
 * @param value
 */
void ModelSource::addDefaults(int role, const QString & value) {
	_defaults.other[role].insert(value);
}

void ModelSource::insertPointer(const FilePointer & pointer) {
	_pointers.push_back(pointer);
	_lastOffset = pointer.offset;
	_lastLine = pointer.line;
}

void ModelSource::updateDefaults(const ModelDefaults & defaults) {
	_defaults = defaults;
}

void ModelSource::cancel() {
	_canceled = true;
}
