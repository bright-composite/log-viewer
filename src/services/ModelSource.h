#ifndef FILEsource_H
#define FILEsource_H

#include <QObject>
#include <QFile>

#include <memory>

#include <adapters/ModelAdapter.h>
#include <models/ModelPatch.h>

class AsyncModelWorker;

struct FilePointer
{
	qint64 offset;
	int line;
};

/**
 * @brief The ModelDefaults struct
 * Stores default values collected by model adapter
 */
struct ModelDefaults
{
	ModelDefaults() : startTime(QDateTime::fromString("")), endTime(QDateTime::fromString("")) {}
	ModelDefaults(const ModelDefaults & d) : startTime(d.startTime), endTime(d.endTime), other(d.other) {}

	QDateTime startTime;
	QDateTime endTime;

	QHash<int, QSet<QString>> other;
};

/**
 * @brief The TempFileRef struct
 * Creates the temporary copy of the given file
 */
struct TempFileRef {
	TempFileRef(const QString & original);
	~TempFileRef();

	static std::shared_ptr<TempFileRef> make(const QString & original) {
		return std::make_shared<TempFileRef>(original);
	}

	QString filePath() const {
		return _filePath;
	}

private:
	QString _filePath;
};

Q_DECLARE_METATYPE(FilePointer)
Q_DECLARE_METATYPE(ModelDefaults)

/**
 * @brief The ModelSource class
 * Used to read files and synchronize loading progress between threads
 */
class ModelSource : public QObject
{
	Q_OBJECT
public:
	explicit ModelSource(const QString & filePath, ModelAdapter * adapter, QObject * parent = nullptr);
	virtual ~ModelSource();

	const FilePointer & pointer(int rowId) const;

	QString filePath() const;

	/**
	 * @brief canFetchMore
	 * @return true if there are available data in the file, false otherwise
	 */
	bool canFetchMore() const;

	QDateTime startTime() const {
		return _defaults.startTime;
	}

	QDateTime endTime() const {
		return _defaults.endTime;
	}

	/**
	 * @brief createLink
	 * Creates a new instance of ModelSource and connects to it to receive updates
	 *
	 * @return new ModelSource instance
	 */
	ModelSource * createLink(QObject * parent);

	/**
	 * @brief getRow
	 * @param int rowId
	 * @return a row obtained from the file or the cache
	 */
	ModelRow getRow(int rowId);

	QByteArray read(int length);

	QByteArray read(qint64 offset, int length);

	void setStartTime(const QDateTime & time) {
		_defaults.startTime = time;
	}

	void setEndTime(const QDateTime & time) {
		_defaults.endTime = time;
	}

	/**
	 * @brief getDefaults
	 * @param role
	 * @return list of default values for the given role
	 */
	QStringList getDefaults(int role) const;

	/**
	 * @brief addDefaults
	 * Adds new default value for the given role
	 *
	 * @param role
	 * @param value
	 */
	void addDefaults(int role, const QString & value);

	int recordCount() const {
		return int(_pointers.size() - 1);
	}

	int lineCount() const {
		return _lastLine - 1;
	}

	int chunkSize() const {
		return _chunkSize;
	}

	void setChunkSize(int);

	/**
	 * @brief load
	 * Reads file structure
	 */
	void fetchMore();

signals:
	/**
	 * @brief pointerInserted
	 * Received by subscribed model sources
	 *
	 * @param offset
	 * @param line
	 */
	void pointerInserted(const FilePointer & pointer);

	/**
	 * @brief defaultsUpdated
	 * Received by subscribed model sources
	 *
	 * @param defaults
	 */
	void defaultsUpdated(const ModelDefaults & defaults);

	/**
	 * @brief fragmentObtained
	 * Received by model workers
	 *
	 * @param patch
	 * @param progress
	 */
	void fragmentObtained(const ModelPatch & patch, float progress);

	/**
	 * @brief loaded
	 * Loading has been finished or canceled
	 */
	void loaded();

public slots:
	void cancel();

protected slots:
	void insertPointer(const FilePointer & pointer);
	void updateDefaults(const ModelDefaults & defaults);

private:
	ModelSource(ModelSource * origin, QObject * parent);

	void processBlock(ModelPatch & patch, const QByteArray & block);

	std::shared_ptr<TempFileRef> _fileRef;
	QFile _file;

	QByteArray _buffer;
	ModelAdapter * _adapter = nullptr;

	std::vector<FilePointer> _pointers;
	qint64 _lastOffset = 0;
	int _lastLine = 0;

	QHash<int, ModelRow> _cache;
	ModelDefaults _defaults;

	qint64 _fileSize = 0;
	int _chunkSize = 1024;
	bool _loaded = false;
	bool _canceled = false;
};

#endif // FILEsource_H
