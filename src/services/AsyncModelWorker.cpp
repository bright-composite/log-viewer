#include "AsyncModelWorker.h"

#include <QDebug>

#include <services/ModelSource.h>

AsyncModelWorker::AsyncModelWorker(EventListModel * model) {
	connect(model, SIGNAL(sourceChanged(ModelSource *)), this, SLOT(updateSource(ModelSource *)));
	connect(model, SIGNAL(reduceTriggered(SortFilterOptions)), this, SLOT(reduce(SortFilterOptions)));
	connect(model, SIGNAL(searchTriggered(QString,bool,bool)), this, SLOT(search(QString,bool,bool)));
	connect(model, SIGNAL(canceled()), this, SLOT(cancel()));
	connect(model, SIGNAL(destroyed(QObject*)), this, SIGNAL(closed()));

	connect(this, SIGNAL(inserted(ModelPatch)), model, SLOT(applyPatch(ModelPatch)));
	connect(this, SIGNAL(occurrenciesFound(SearchResults)), model, SLOT(processResults(SearchResults)));

	connect(this, SIGNAL(started()), model, SLOT(clear()));
	connect(this, SIGNAL(progressChanged(float)), model, SLOT(setProgress(float)));
	connect(this, SIGNAL(closed()), this, SLOT(deleteLater()));
}

void AsyncModelWorker::updateSource(ModelSource * source) {
	cancel();
	_loaded = false;

	if(_source != nullptr) {
		_source->deleteLater();
		_source = nullptr;
	}

	if(source != nullptr) {
		// Create source copy and load file

		_source = source->createLink(this);

		connect(_source, SIGNAL(fragmentObtained(ModelPatch,float)), this, SLOT(recordsProcessed(ModelPatch,float)));
		connect(this, SIGNAL(canceled()), _source, SLOT(cancel()));

		emit started();
		emit progressChanged(0);

		_source->fetchMore();

		// Determine acceptable chunk size
		_chunkSize = std::max(4, std::min(_source->recordCount() / 128, 256));

		emit finished();
		_loaded = true;
	}
}

void AsyncModelWorker::start() {
	_current = 0;
	_filtered = 0;
	_indices.clear();

	emit progressChanged(0);

	_timer.start();
}

void AsyncModelWorker::finish() {
	qDebug() << "finished async operation";

	_timer.stop();
	_timer.disconnect(this);

	emit progressChanged(1.0f);
	emit finished();
}

void AsyncModelWorker::cancel() {
	qDebug() << "cancelled async operation";

	_timer.stop();
	_timer.disconnect(this);

	emit progressChanged(1.0f);
	emit canceled();
	emit finished();
}

void AsyncModelWorker::close() {
	qDebug() << "closed";
	emit closed();
}

void AsyncModelWorker::recordsProcessed(const ModelPatch & patch, float progress) {
	emit progressChanged(progress);
	emit inserted(patch);
}

void AsyncModelWorker::reduce(const SortFilterOptions & options) {
	if(!_loaded) {
		return;
	}

	if(_timer.isActive()) {
		cancel();
	}

	_options = options;

	// Select optimal processing way

	_stepSize = std::max(64, std::min(_chunkSize / 4, 128));

	if(_options.filter.isEmpty() && !_options.startTime.isValid() && !_options.endTime.isValid()) {
		if((_options.sortRole == ModelAdapter::LineNumber || _options.sortRole == -1) && _options.sortOrder == Qt::AscendingOrder) {
			qDebug() << "start async reduce";
			_stepSize = _source->recordCount();
			_processor = proceed;
		} else {
			qDebug() << "start async reduce with sorting";
			_processor = proceedWithSorting;
		}
	} else {
		if((_options.sortRole == ModelAdapter::LineNumber || _options.sortRole == -1) && _options.sortOrder == Qt::AscendingOrder) {
			qDebug() << "start async reduce with filtering";
			_processor = proceedWithFiltering;
		} else {
			qDebug() << "start async reduce with filtering and sorting";
			_processor = proceedWithFilteringAndSorting;
		}
	}

	connect(&_timer, SIGNAL(timeout()), this, SLOT(reduceNext()));
	emit started();
	start();
}

void AsyncModelWorker::search(const QString & text, bool regexp, bool caseSensitive) {
	if(!_loaded) {
		return;
	}

	if(_timer.isActive()) {
		cancel();
	}

	qDebug() << "start search";

	if(regexp) {
		_regexp = QRegExp(text, caseSensitive ? Qt::CaseSensitive : Qt::CaseInsensitive, QRegExp::RegExp);
	} else {
		_regexp = QRegExp(QString(text).replace("  ", " "), caseSensitive ? Qt::CaseSensitive : Qt::CaseInsensitive, QRegExp::FixedString);
	}

	connect(&_timer, SIGNAL(timeout()), this, SLOT(searchNext()));
	start();
}

/**
 * @brief reduceNext
 * Processes rows
 */
void AsyncModelWorker::reduceNext() {
	ModelPatch patch;

	for(int i = 0; i < _stepSize && _current < _source->recordCount(); ++i) {
		_processor(this, patch, _current);
		++_current;
	}

	emit progressChanged(float(_current) / float(_source->recordCount()));

	if(patch.size() > 0) {
		emit inserted(patch);
	}

	if(_current == _source->recordCount()) {
		finish();
	}
}

/**
 * @brief searchNext
 * Processes the next piece of data to find requested entries
 */
void AsyncModelWorker::searchNext() {
	SearchResults results;

	for(int i = 0; i < _stepSize && _current < _source->recordCount(); ++i) {
		auto row = _source->getRow(_current);

		for(auto i = row.begin(); i != row.end(); ++i) {
			auto value = i.value().toString().replace("  ", " ");
			int pos = 0;

			while ((pos = _regexp.indexIn(value, pos)) != -1) {
				results.add(_current, i.key(), pos, pos + _regexp.matchedLength());
				pos += _regexp.matchedLength();
			}
		}

		++_current;
	}

	emit progressChanged(float(_current) / float(_source->recordCount()));

	if(results.size() > 0) {
		emit occurrenciesFound(results);
	}

	if(_current == _source->recordCount()) {
		finish();
	}
}

/**
 * @brief proceed
 * Processes rows without filtering and sorting - the most optimal case
 */
void AsyncModelWorker::proceed(AsyncModelWorker *, ModelPatch & patch, int rowId) {
	patch.add(rowId,  rowId);
}

/**
 * @brief proceedWithFiltering
 * Processes rows with filtering but without sorting
 */
void AsyncModelWorker::proceedWithFiltering(AsyncModelWorker * worker, ModelPatch & patch, int rowId) {
	auto row = worker->_source->getRow(rowId);

	if(worker->acceptRow(row)) {
		patch.add(worker->_filtered, rowId);
		++worker->_filtered;
	}
}

/**
 * @brief proceedWithSorting
 * Processes rows with sorting but without filtering
 */
void AsyncModelWorker::proceedWithSorting(AsyncModelWorker * worker, ModelPatch & patch, int rowId) {
	auto row = worker->_source->getRow(rowId);
	patch.add(worker->placeRow(row, rowId), rowId);
}

/**
 * @brief proceedWithSorting
 * Processes rows with sorting and filtering
 */
void AsyncModelWorker::proceedWithFilteringAndSorting(AsyncModelWorker * worker, ModelPatch & patch, int rowId) {
	auto row = worker->_source->getRow(rowId);

	if(worker->acceptRow(row)) {
		patch.add(worker->placeRow(row, rowId), rowId);
	}
}

/**
 * @brief acceptRow
 * Checks whether the row matches the specified filtering conditions
 *
 * @param ModelRow row
 * @return true if row matches, false otherwise
 */
bool AsyncModelWorker::acceptRow(const ModelRow & row) {
	auto time = row[ModelAdapter::Time];

	if(time.isValid()) { // filter by time bounds
		auto dt = time.toDateTime();

		if(dt.isValid()) {
			if(_options.startTime.isValid() && dt < _options.startTime) {
				return false;
			}

			if(_options.endTime.isValid() && dt > _options.endTime) {
				return false;
			}
		}
	}

	for(auto iter = _options.filter.constBegin(); iter != _options.filter.constEnd(); ++iter) {
		auto & entry = iter.value();
		QRegExp regexp(entry.expr, entry.sensitivity, QRegExp::PatternSyntax(entry.syntax));

		if(regexp.indexIn(row[iter.key()].toString().replace("  ", " ")) == -1) {
			return false;
		}
	}

	return true;
}

/**
 * @brief placeRow
 * Inserts the given row to
 * @param row
 * @param rowId
 * @return an index where row was inserted to
 */
int AsyncModelWorker::placeRow(const ModelRow & row, int rowId) {
	int dest = indexToInsert(row, 0, _indices.size());
	_indices.insert(dest, rowId);

	return dest;
}

/**
 * @brief indexToInsert
 * Performs binary search for a place to the next row
 *
 * @param row
 * @param from
 * @param to
 * @return a place where the row will be inserted to
 */
int AsyncModelWorker::indexToInsert(const ModelRow & row, int begin, int end) {
	if(begin == end) {
		return begin;
	}

	int middle = int(round((begin + end - 1) / 2.0f));

	if(isLessThan(row, _source->getRow(_indices[middle]))) {
		return indexToInsert(row, begin, middle);
	}

	return indexToInsert(row, middle + 1, end);
}

/**
 * @brief isLessThan
 * Compares two rows using the sorting options
 * @param a
 * @param b
 * @return true - if a < b and sort order is ascending or b < a and sort order is descending
 *		   false - otherwise
 */
bool AsyncModelWorker::isLessThan(const ModelRow & a, const ModelRow & b) {
	auto fa = a[_options.sortRole];
	auto fb = b[_options.sortRole];

	return (fa < fb && _options.sortOrder == Qt::AscendingOrder) || (fa > fb && _options.sortOrder == Qt::DescendingOrder);
}
