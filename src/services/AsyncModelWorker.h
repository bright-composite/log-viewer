#ifndef ASYNCMODELWORKER_H
#define ASYNCMODELWORKER_H

#include <QFile>
#include <QTimer>
#include <QThread>

#include <models/EventListModel.h>

/**
 * @brief The AsyncModelWorker class
 * Used to perform asynchronous tasks: file loading, filtering, sorting, searching
 */
class AsyncModelWorker : public QObject
{
	Q_OBJECT
public:
	explicit AsyncModelWorker(EventListModel * model);

	/**
	 * @brief run
	 * Creates and launches worker in a separate thread
	 *
	 * @param model
	 * @param chunkSize
	 */
	static void launch(EventListModel * model) {
		auto * thread = new QThread();

		QObject::connect(thread, &QThread::started, [=]() {
			auto * worker = new AsyncModelWorker(model);
			QObject::connect(worker, SIGNAL(closed()), thread, SLOT(quit()));
		});

		thread->start();
	}

signals:
	void loaded();
	void started();
	void canceled();
	void finished();
	void closed();
	void progressChanged(float progress);

	void inserted(const ModelPatch & patch);
	void occurrenciesFound(const SearchResults & results);

public slots:
	void reduce(const SortFilterOptions & options);
	void search(const QString & text, bool regexp, bool caseSensitive);

protected slots:
	void updateSource(ModelSource * source);
	void recordsProcessed(const ModelPatch & patch, float progress);

	void start();
	void finish();
	void cancel();
	void close();

	/**
	 * @brief reduceNext
	 * Processes rows
	 */
	void reduceNext();

	/**
	 * @brief searchNext
	 * Processes the next piece of data to find requested entries
	 */
	void searchNext();

protected:
	using Processor = void(*)(AsyncModelWorker *, ModelPatch &, int);

	/**
	 * @brief proceed
	 * Processes rows without filtering and sorting - the most optimal case
	 */
	static void proceed(AsyncModelWorker * worker, ModelPatch & patch, int rowId);

	/**
	 * @brief proceedWithFiltering
	 * Processes rows with filtering but without sorting
	 */
	static void proceedWithFiltering(AsyncModelWorker * worker, ModelPatch & patch, int rowId);

	/**
	 * @brief proceedWithSorting
	 * Processes rows with sorting but without filtering
	 */
	static void proceedWithSorting(AsyncModelWorker * worker, ModelPatch & patch, int rowId);

	/**
	 * @brief proceedWithSorting
	 * Processes rows with sorting and filtering
	 */
	static void proceedWithFilteringAndSorting(AsyncModelWorker * worker, ModelPatch & patch, int rowId);

	/**
	 * @brief acceptRow
	 * Checks whether the row matches the specified filtering conditions
	 *
	 * @param ModelRow row
	 * @return true if row matches, false otherwise
	 */
	bool acceptRow(const ModelRow & row);

	/**
	 * @brief placeRow
	 * Inserts the given row to
	 *
	 * @param row
	 * @param rowId - original row identifier
	 * @return an index where the row was inserted to
	 */
	int placeRow(const ModelRow & row, int rowId);

	/**
	 * @brief indexToInsert
	 * Performs binary search for a place to the next row
	 *
	 * @param row
	 * @param from
	 * @param to
	 * @return a place where the row will be inserted to
	 */
	int indexToInsert(const ModelRow & row, int from, int to);

	/**
	 * @brief isLessThan
	 * Compares two rows using the sorting options
	 *
	 * @param a
	 * @param b
	 * @return true - if a < b and sort order is ascending or b < a and sort order is descending
	 *		   false - otherwise
	 */
	bool isLessThan(const ModelRow & a, const ModelRow & b);

	QTimer _timer;
	int _stepSize = 64;
	int _chunkSize;
	int _current = 0;

	ModelSource * _source = nullptr;

	QVector<int> _indices;
	int _filtered = 0;

	Processor _processor = AsyncModelWorker::proceed;

	SortFilterOptions _options;
	QRegExp _regexp;
	bool _loaded = false;
};

#endif // ABSTRACTMODELWORKER_H
