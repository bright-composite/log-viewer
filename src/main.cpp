#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QStyleHints>

#include <models/EventListModel.h>
#include <adapters/ApacheErrorLogAdapter.h>
#include <services/ModelSource.h>

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	qRegisterMetaType<FilePointer>();
	qRegisterMetaType<ModelPatch>();
	qRegisterMetaType<ModelDefaults>();
	qRegisterMetaType<SortFilterOptions>();
	qRegisterMetaType<SearchResults>();

	qmlRegisterType<ModelAdapter>("test.logs", 1, 0, "ModelAdapter");
	qmlRegisterType<ApacheErrorLogAdapter>("test.logs", 1, 0, "ApacheErrorLogAdapter");
	qmlRegisterType<EventListModel>("test.logs", 1, 0, "EventListModel");

	QQmlApplicationEngine engine;
	engine.addImportPath("qrc:/");
	engine.addImportPath("qrc:/models");
	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

	return app.exec();
}
