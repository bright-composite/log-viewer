#ifndef FIELDPARSEEXCEPTION_H
#define FIELDPARSEEXCEPTION_H

#include <QString>

/**
 * @brief The FieldParseException class
 * Thrown when some field in the log record can not be parsed
 */
class FieldParseException
{
public:
	FieldParseException(const QString & field) : _field(field) {}

	const QString & field() const {
		return _field;
	}

private:
	QString _field;
};

#endif // FIELDPARSEEXCEPTION_H
