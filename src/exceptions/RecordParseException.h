#ifndef RECORDPARSEEXCEPTION_H
#define RECORDPARSEEXCEPTION_H

#include <QString>

/**
 * @brief The RecordParseException class
 * Thrown when an event can not be parsed from a log record
 */
class RecordParseException
{
public:
	RecordParseException(int line = -1, const QString & object = "record") : _line(line), _object(object) {}

	int line() const {
		return _line;
	}

	const QString & object() const {
		return _object;
	}

private:
	int _line;
	QString _object;
};

#endif // LOGPARSEEXCEPTION_H
