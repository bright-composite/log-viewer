import QtQuick 2.6
import QtTest 1.1

import "../src/models"

TestCase {
    Item {
        id: test
        property date date: DateTime.dateTime()
        property string text: DateTime.fullDateTimeToString(DateTime.dateTime())

        PropertyConnection {
            targetProperty: "date"
            function map(value) { return DateTime.parse(value) }

            sourceProperty: "text"
            function unmap(value) { return DateTime.stringify(value) }
        }
    }

    function test_sync_target() {
        var date = new Date(1970, 0, 1, 23, 45, 19, 495)
        test.text = DateTime.stringify(date)
        compare(test.date, date)
    }

    function test_sync_source() {
        var date = new Date(1970, 0, 1, 23, 45, 19, 495)
        test.date = date
        compare(test.text, DateTime.stringify(date))
    }
}
