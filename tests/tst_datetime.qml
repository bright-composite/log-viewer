import QtQuick 2.6
import QtTest 1.1

import "../src/models"

TestCase {
    property date givenDateTime: new Date(1970, 1, 1, 23, 45, 19, 495)

    function test_date() {
        var date = DateTime.date(givenDateTime)
        compare(date.year, 1970); compare(date.month, 1); compare(date.day, 1)
    }

    function test_time() {
        var time = DateTime.time(givenDateTime)
        compare(time.hour, 23); compare(time.minute, 45); compare(time.second, 19); compare(time.millisecond, 495)
    }

    function test_date_time() {
        var dateTime = DateTime.dateTime(DateTime.date(1970, 1, 1), DateTime.time(23, 45, 19, 495))
        compare(dateTime, givenDateTime)
    }
}
